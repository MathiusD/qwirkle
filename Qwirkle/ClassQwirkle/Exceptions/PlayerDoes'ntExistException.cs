﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle.Exceptions
{
    [Serializable]
    public class PlayerDoes_ntExistException : Exception
    {
        public PlayerDoes_ntExistException() : base("Player doesn't exist.")
        {
            /*
            Constructeur de l'Exception PlayerDoes'ntExistException.
            Cette exception est appelée dans le cas où on l'on passe un
            Joueur qui n'appartient pas au bord en paramètre
            d'une méthode influant sur celui-ci ou dans le cas où l'on
            fait appel d'un joueur une méthode avec un board auquel
            il n'appartient pas.
            Celle-ci indique en message "Ce Joueur n'existe pas".
            */
        }
    }
}
