﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle.Exceptions
{
    [Serializable]
    public class WrongNumberOfPlayerException : Exception
    {
        public WrongNumberOfPlayerException(short nb) : base (string.Format("Wrong number of players. You have entered {0} Players, excluding {0} is not between 2 and 4.", nb))
        {
            /*
            Constructeur de l'Exception WrongNumberOfPlayerException.
            Cette exception est appelée dans le cas où l'on tente
            de construire un Board avec moins de 2 Joueurs ou plus
            de 4 Joueurs.
            Celle-ci indique en message "Mauvais nombre de Joueurs. 
            Vous avez renseigné nb joueurs, hors nb n'est pas compris
            entre 2 et 4." avec nb étant le short passé en argument.
            */
        }
        public WrongNumberOfPlayerException() : base("Wrong number of player.")
        {
            /*
            Second Constructeur de l'Exception WrongNumberOfPlayerException.
            Celle-ci indique en message "Mauvais nombre de Joueurs.".
            */
        }
    }
}
