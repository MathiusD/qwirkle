﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle.Exceptions
{
    [Serializable]
    public class NoTilePlacedInThisTurnException : Exception
    {
        public NoTilePlacedInThisTurnException() : base("No Tile placed in this turn.")
        {
            /*
            Constructeur de l'Exception NoTilePlacedInThisTurnException.
            Cette exception est appelée dans le cas où on l'on tente d'annuler
            le placement d'un tour alors qu'aucune tuile n'a été posée.
            Celle-ci indique en message "Aucune tuile posé ce tour".
            */
        }
    }
}
