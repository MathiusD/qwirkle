﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle.Exceptions
{
    [Serializable]
    public class BadParametersForTileConstructorException : Exception
    {
        public BadParametersForTileConstructorException() : base("Bad Parameter For Tile Constructor.")
        {
            /*
            Constructeur de l'Exception BadParametersForTileConstructorException.
            Cette exception est appelée dans le cas où on l'on passe de mauvais
            arguments en paramètre du Constructeur de Tuiles.
            Celle-ci indique en message "Mauvais Paramètres pour le Constructeur
            de Tuiles".
            */
        }
    }
}
