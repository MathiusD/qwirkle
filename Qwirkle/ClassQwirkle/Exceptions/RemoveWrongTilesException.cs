﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle.Exceptions
{
    [Serializable]
    public class RemoveWrongTilesException : Exception
    {
        public RemoveWrongTilesException() : base("Tile is'nt in this rack.")
        {
            /*
            Constructeur de l'Exception RemoveWrongTilesException.
            Cette exception est appelée dans le cas où on l'on passe
            une Tuile en argument alors qu'elle n'est pas présente
            au sein du stack dont on tente de l'ôter.
            Celle-ci indique en message "La Tuile n'est pas dans ce
            rack".
            */
        }

    }
}
