﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle.Exceptions
{
    [Serializable]
    public class TileDoes_ntExistException : Exception
    {
        public TileDoes_ntExistException() : base("This Tile Does'nt Exist in this Board")
        {
            /*
            Constructeur de l'Exception TileDoes'ntExistException.
            Cette exception est appelée dans le cas où on l'on
            cherche une tuile qui n'existe pas au sein d'un board.
            Celle-ci indique en message "Cette tuile n'existe pas
            au sein de ce board".
            */
        }
    }
}
