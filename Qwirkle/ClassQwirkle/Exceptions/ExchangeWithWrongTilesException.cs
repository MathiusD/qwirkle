﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle.Exceptions
{
    [Serializable]
    public class ExchangeWithWrongTilesException : Exception
    {
        public ExchangeWithWrongTilesException() : base("Exchange with Wrong Tiles.")
        {
            /*
            Constructeur de l'Exception ExchangeWithWrongTilesException.
            Cette exception est appelée dans le cas où on l'on passe une
            liste de Tuiles qui n'appartient pas au joueur en paramètre
            de la méthode Trade_Tiles.
            Celle-ci indique en message "Echange avec des Mauvaises Tuiles".
            */
        }
    }
}
