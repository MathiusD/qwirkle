﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ClassQwirkle;

namespace Qwirkle.Exceptions
{
    [Serializable]
    public class WrongPositionForTileException : Exception
    {
        private string complement;
        private Board board;
        private Tile tile;
        private short x;
        private short y;
        public WrongPositionForTileException() : base("Tile is'nt placed to valide position.")
        {
            /*
            Constructeur de l'Exception WrongPositionForTileException.
            Cette exception est appelée dans le cas où on l'on tente
            de placer une tuile via la méthode Place_Tile à une 
            position non valide.
            Celle-ci indique en message "La Tuile n'est pas placée
            à une position valide".
            Et pour ce constructeur, le complément est initié à "
            Pas de complément pour cette Exception".
            Cependant les variables board, tile, x et y ne sont pas
            renseignées.
            */
            this.complement = "No complement for this Exception.";
        }
        public WrongPositionForTileException(string complement) : base("Tile is'nt placed to valide position.")
        {
            /*
            Second Constructeur de l'Exception
            WrongPositionForTileException.
            Celle-ci indique en message "La Tuile n'est pas placée
            à une position valide".
            Et pour ce constructeur, le complément est initié à la
            valeur de la chaine de caractères passée en argument.
            Cependant les variables board, tile, x et y ne sont pas
            renseignées.
            */
            this.complement = complement;
        }
        public WrongPositionForTileException(Board board, Tile tile, short x, short y) : base("Tile is'nt placed to valide position.")
        {
            /*
            Troisième Constructeur de l'Exception
            WrongPositionForTileException.
            Celle-ci indique en message "La Tuile n'est pas placée
            à une position valide".
            Et pour ce constructeur, le complément est initié à "
            Pas de complément pour cette Exception".
            Cependant les variables board, tile, x et y sont
            intialisées avec les variables passées en paramètres.
            */
            this.complement = "No complement for this Exception.";
            this.board = board;
            this.tile = tile;
            this.x = x;
            this.y = y;
        }
        public WrongPositionForTileException(string complement, Board board, Tile tile, short x, short y) : base("Tile is'nt placed to valide position.")
        {
            /*
            Second Constructeur de l'Exception
            WrongPositionForTileException.
            Celle-ci indique en message "La Tuile n'est pas placée
            à une position valide".
            Et pour ce constructeur, le complément est initié à la
            valeur de la chaine de caractères passée en argument.
            Cependant les variables board, tile, x et y sont
            intialisées avec les variables passées en paramètres.
            */
            this.complement = complement;
            this.board = board;
            this.tile = tile;
            this.x = x;
            this.y = y;
        }
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            /*
            Surcharge de ma méthode GetObjectData.
            */
            base.GetObjectData(info, context);
        }
        public string GetComplement()
        {
            /*
            Accesseur du string complement.
            */
            return complement;
        }
        public Board GetBoard()
        {
            /*
            Accesseur du Board board.
            */
            return board;
        }
        public Tile GetTile()
        {
            /*
            Accesseur de la Tile tile.
            */
            return tile;
        }
        public int GetX()
        {
            /*
            Accesseur de l'int x.
            */
            return x;
        }
        public int GetY()
        {
            /*
            Accesseur de l'int y.
            */
            return y;
        }
    }
}
