﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle.Exceptions
{
    [Serializable]
    public class TileOutOfLimitsException : Exception
    {
        public TileOutOfLimitsException() : base("Tile is Out Of Limit of Board.")
        {
            /*
            Constructeur de l'Exception TileOutOfLimitsException.
            Cette exception est appelée dans le cas où on l'on
            tente de placer une tuile en dehors des limites du
            plateau.
            Celle-ci indique en message "Tuile en dehors des limites
            du plateau".
            */
        }
    }
}
