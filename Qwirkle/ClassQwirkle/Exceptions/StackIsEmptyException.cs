﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle.Exceptions
{
    [Serializable]
    public class StackIsEmptyException :Exception
    {
        public StackIsEmptyException() : base("Stack is Empty.")
        {
            /*
            Constructeur de l'Exception StackIsEmptyException.
            Cette exception est appelée dans le cas où on l'on tente
            de prendre une Tuile alors que le stack est vide.
            Celle-ci indique en message "Le stack est vide".
            */
        }
    }
}
