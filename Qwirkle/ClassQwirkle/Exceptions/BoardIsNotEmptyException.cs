﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle.Exceptions
{
    [Serializable]
    public class BoardIsNotEmptyException : Exception
    {
        public BoardIsNotEmptyException() : base("The Board Is Not Empty.")
        {
            /*
            Constructeur de l'Exception BoardIsNotEmptyException.
            Cette exception est appelée dans le cas où l'on tente de
            de modifier les valeurs du nombre de ligne ou de colonne
            et que le board n'est pas vide.
            Celle-ci indique en message "Le plateau n'est pas vide".
            */
        }
    }
}
