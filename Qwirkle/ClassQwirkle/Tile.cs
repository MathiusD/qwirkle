﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qwirkle.Exceptions;

namespace ClassQwirkle
{
    public class Tile
    {
        /*
        Définition des composantes de la classe Tiles
        Les shorts color et form allant de 0 à 5 pour les tuiles
        "classiques" indique quelle est l'aspect visuel de la tuile.
        Le short id quand à lui allant de 0 à 2 pour les tuiles
        "classiques" permet d'avoir des tuiles uniques entre elles
        même si il y a dans chaque pioche 3 fois une tuile d'une couleur
        et d'une forme précise.
        Les trois shorts peuvent être égaux à -1 simultanément et en ce cas
        c'est une tuile "vide" c'est juste un élément tuile qui n'en n'est pas
        une tuile mais plus remplir le board pour indiquer que la case sera
        vide.
        */
        private short id;
        private short color;
        private short shape;
        public Tile(short id, short color, short shape)
        {
            /*
            Premier Constructeur de la classe Tile.
            Ce constructeur crée une tuile en fonction des shorts passés en
            paramètres.
            Le constructeur attends un id compris entre 0 et 2, une forme
            comprise entre 0 et 5 et une couleur comprise entre 0 et 5.
            Cependant, si les paramètres ne correspondent pas aux attentes
            du constructeur, il renvoie une Exception affirmant "Mauvais
            Paramètres pour le Constructeur de Tuiles".
            */
            if (id < 3 && id >= 0 && color >= 0 && color < 6 && shape >= 0 && shape < 6)
            {
                this.shape = shape;
                this.color = color;
                this.id = id;
            } else
            {
                throw new BadParametersForTileConstructorException();
            }
        }
        public Tile()
        {
            /*
            Second Constructeur de la classe Tile.
            Ce constructeur crée la tuile "vide" évoqué plus haut.
            En conséquence, il initie les 3 shorts à -1.
            */
            this.shape = -1;
            this.id = -1;
            this.color = -1;
        }
        public short getId()
        {
            /*
            Accesseur du short id.
            */
            return this.id;
        }
        public short getColor()
        {
            /*
            Accesseur du short color.
            */
            return this.color;
        }
        public short getShape()
        {
            /*
            Accesseur du short form.
            */
            return this.shape;
        }
        public bool Is_similar(Tile other)
        {
            /*
            Methode Is_similar().
            Cette méthode renvoie un booléen en fonction de la
            tuile passée en paramètre, si celle-ci est similaire
            donc avec 1 couleur OU 1 forme en commun avec la méthode
            depuis laquelle la méthode est appellée, la méthode
            renvoie true dans le cas contraire elle renvoie false.
            */
            if (this.color == other.color && this.shape != other.shape || this.color != other.color && this.shape == other.shape)
                return true;
            else
                return false;
        }
        public override int GetHashCode()
        {
            /*
            Surdéfinition de la methode GetHashCode().
            */
            int hash;
            int.TryParse(string.Format("{0}{1}{2}", this.id+2, this.shape+2, this.color+2), out hash);
            return hash;
        }
        public static bool operator ==(Tile tile1, Tile tile2)
        {
            /*
            Surdéfinition de l'opérateur ==.
            */
            if (tile1.GetHashCode() == tile2.GetHashCode())
            {
                return true;
            } else
            {
                return false;
            }
        }
        public static bool operator !=(Tile tile1, Tile tile2)
        {
            /*
            Surdéfinition de l'opérateur !=.
            */
            return !(tile1 == tile2);
        }
        public override bool Equals(object obj)
        {
            /*
            Surdéfinition de la méthode Equals().
            */
            return obj is Tile && this == (Tile)obj;
        }
    }
}