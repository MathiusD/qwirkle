﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qwirkle.Exceptions;

namespace ClassQwirkle
{
    public class Player
    {
        /*
        Définition des composantes de la classe Player
        La string color permet de pouvoir personnaliser à terme l'affichage
        du nom de chaque joueur avec soit des valeur hexadécimale soit des 
        couleurs prédéfinies.
        La string name définie le nom du joueur.
        Le short age définit l'age du joueur.
        L'entier score renseigne le score du joueur.
        La liste unidirectionnelle de tuiles rack est la main du joueur où
        sont stockées les tuiles qu'il possède et qu'il peut jouer.
        */
        private string color;
        private string name;
        private short age;
        private int score;
        private List<Tile> rack = new List<Tile>();
        public Player (string color, string name, short age)
        {
            /*
            Constructeur de la classe Player.
            Ce constructeur crée un joueur avec les paramètres color,
            age et name initiés avec les paramètres fournis en
            paramètres.
            Il initie également le score à 0 et laisse le rack vide.
            */
            this.color = color;
            this.name = name;
            this.age = age;
            this.score = 0;
        }
        public string getColor()
        {
            /*
            Accesseur de la chaîne de caractères color.
            */
            return color;
        }
        public string getName()
        {
            /*
            Accesseur de la chaîne de caractères name.
            */
            return name;
        }
        public short getAge()
        {
            /*
            Accesseur du short age.
            */
            return age;
        }
        public int getScore()
        {
            /*
            Accesseur de l'entier score.
            */
            return score;
        }
        public List<Tile> getRack()
        {
            /*
            Accesseur de la liste de tuiles rack.
            */
            return this.rack;
        }
        public void AddRack(Tile tile)
        {
            /*
            Methode AddTile().
            Cette méthode prend en argument une tuile et l'ajoute
            à la main du joueur.
            */
            this.rack.Add(tile);
        }
        public void Increase_score(short increma)
        {
            /*
            Methode Increase_score().
            Cette méthode prends en argument un entier et incrémente
            le score du joueur en question.
            */
            this.score = this.score + increma;
        }
        public short Missing_Tiles()
        {
            /*
            Methode Missing_Tiles().
            Cette méthode renvoie un short indiquant le nombre de pièces
            manquantes dans le rack du joueur.
            */
            return (short)(6 - this.rack.Count);
        }
        public void Draw(Board Game)
        {
            /*
            Methode Draw().
            Cette méthode utilise la méthode Missing_Tiles() sur le joueur
            qui pioche afin de piocher un nombre de tuiles équivalent au 
            nombre de tuiles manquantes tout en les retirants du plateau
            passé en paramètre.
            */
            if (Game.Player_Exist(this) == true)
            {
                short nbmax = this.Missing_Tiles();
                if (nbmax > 0)
                {
                    Random r = new Random();
                    for (short i = 0; i < nbmax; i++)
                    {
                        short rand = (short)(r.Next(Game.getStack().Count));
                        if (Game.Is_Stack_Empty() == false)
                        {
                            this.AddRack(Game.getStack()[rand]);
                            Game.RemoveStack(Game.getStack()[rand]);
                        } else
                        {
                            throw new StackIsEmptyException();
                        }
                    }
                }
            } else
            {
                throw new PlayerDoes_ntExistException();
            }

        }
        public void Trade_Tiles(Board Game, List<Tile> Exchange_Tiles)
        {
            /*
            Methode Trad_Tiles().
            Cette méthode vérifie en premier lieu que la liste de tuiles à
            échanger, qui est passée en argument de la méthode, est bien
            contenue dans le rack du joueur, avant de lui enlever les tuiles
            en question.
            Si l'une des tuiles n'est pas contenue au sein du rack, une
            Exception est lancée affirmant "Echange avec de Mauvaises Tuiles"
            Ensuite la méthode fait appel à la méthode Draw() avant d'achever
            son excution en replaçant alors les tuiles passées en paramètres
            dans le sac passé en paramètre.
            */
            if (Game.Player_Exist(this) == true)
            {
                int nbmax = Exchange_Tiles.Count;
                for (short i = 0; i < nbmax; i++)
                {
                    short j = 0;
                    while ((j < (rack.Count - 1)) && (!(rack[j].Equals(Exchange_Tiles[i]))))
                    {
                        j = (short)(j + 1);
                    }
                    if (rack[j].Equals(Exchange_Tiles[i]))
                    {
                        rack.RemoveAt(j);
                    }
                    else
                    {
                        throw new ExchangeWithWrongTilesException();
                    }
                }
                bool exception = false;
                try
                {
                    this.Draw(Game);
                } catch (StackIsEmptyException)
                {
                    exception = true;
                    for (short j = 0; j < Exchange_Tiles.Count; j++)
                    {
                        Game.AddStack(Exchange_Tiles[j]);
                    }
                }
                if (exception == false)
                {
                    for (short j = 0; j < Exchange_Tiles.Count; j++)
                    {
                        Game.AddStack(Exchange_Tiles[j]);
                    }
                } else
                {
                    this.Draw(Game);
                }
            }
            else
            {
                throw new PlayerDoes_ntExistException();
            }
        }

        public void Place_Tiles(Board Game, Tile tile, int x, int y)
        {
            /*
            Methode Place_Tiles().
            Cette Méthode permet le placement d'une tuile à une
            position donnée, si le placement n'est pas valide
            suite à l'appel de la Méthode Rules_Verification(),
            la méthode Place_Tiles() lancera une Exception
            indiquant "Le Placement de la Tuile n'est pas valide",
            si le placement est valide, la tuile est alors placée
            à 2 endroits, dans le "buffer" placed_tiles, qui est
            une liste de Tuiles et sur le plateau aux coordonnées
            passées en arguments.
            */
            if (Game.Player_Exist(this) == true)
            {
                if (Game.Rules_Verification(tile, (short) x, (short)y) == true)
                {
                    this.rack.Remove(tile);
                    Game.AddTile(tile, (short)x, (short)y);
                } else
                {
                    throw new WrongPositionForTileException();
                }
            }
            else
            {
                throw new PlayerDoes_ntExistException();
            }
        }
        public override int GetHashCode()
        {
            /*
            Surdéfinition de la methode GetHashCode().
            */
            int hash = 13;
            hash ^= this.name.GetHashCode();
            hash ^= this.color.GetHashCode();
            hash ^= this.age.GetHashCode();
            hash ^= this.score.GetHashCode();
            for (short i = 0; i < rack.Count; i++)
            {
                hash ^= this.rack[i].GetHashCode();
            }
            return hash;
        }
        public static bool operator ==(Player player1, Player player2)
        {
            /*
            Surdéfinition de l'opérateur ==.
            */
            if (player1.GetHashCode() == player2.GetHashCode())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool operator !=(Player player1, Player player2)
        {
            /*
            Surdéfinition de l'opérateur !=.
            */
            return !(player1 == player2);
        }
        public override bool Equals(object obj)
        {
            /*
            Surdéfinition de la méthode Equals().
            */
            return obj is Player && this == (Player)obj;
        }
    }
}