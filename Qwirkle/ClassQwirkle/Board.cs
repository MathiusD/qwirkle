﻿using Qwirkle.Exceptions;
using System;
using System.Collections.Generic;

namespace ClassQwirkle
{
    public class Board
    {
        /*
        Définition des composantes de la classe Board
        Le stack est la réserve de pions est une liste unidirectionnelle.
        Les shorts nbcolumn et nbline sont des variables indiquant
        le nombre de colones et de lignes actuellement disponible.
        Le board est le plateau de jeu qui est donc une liste d'une liste
        de tuile afin de faire une liste bidirectionnelle de tuiles afin
        d'avoir un stockage du plateau sur deux dimensions tout en
        ayant la possibilité d'utiliser la gestion dynamique des 
        dimension que nous permettent les listes.
        Les shorts nbcolumndisplay et nblinedisplay sont des variables
        indiquant le nombre de colones et de lignes a afficher sur l'interface
        graphique.
        Les shorts boardsizex et y renseignent la taille du plateau (en px).
        Les shorts tilesizex et y renseignent la taille maximale des 
        tuiles (en px).
        La liste unidirectionnelle de tuile placed_tiles est un "buffer"
        de tuiles où seront placé les tuiles dont le pause n'est pas
        encore finalisée (j'entend par la, la pause effective des tuiles
        au sein du buffer). C'est donc le seul moment où les tuiles
        sont "dupliquées".
        La liste unidirectionnelle de joueurs players renseigne les joueurs
        appartenant à la partie, ce qui nous permet de vérifier que les 
        joueurs qui tentent d'intéragir avec ce plateau y jouent 
        effectivement. Cette liste est également ordonnée dans l'ordre
        dans lequel, les joueurs sont censés jouer.
        */
        private List<Tile> stack = new List<Tile>();
        private short nbcolumn;
        private short nbline;
        private short nbcolumndisplay;
        private short nblinedisplay;
        private List<List<Tile>> board = new List<List<Tile>>();
        private short boardsizex;
        private short boardsizey;
        private short tilesizex;
        private short tilesizey;
        private List<Tile> placed_tiles = new List<Tile>();
        private List<Player> players = new List<Player>();
        public Board(List<Player> players)
        {
            /*
            Constructeur de la classe Board.
            Il remplit le stack avec les 108 tuiles.
            Il initie également la largeur et la longueur du plateau à 400px.
            Il initie également le nombre de colone et de ligne à 1.
            Il initie également le nombre de colone et de ligne affichées à 10.
            Il initie également la taille maximale (en x et en y) des tuiles à 40.
            Il initie également la liste des joueurs avec la liste passée en paramètre
            dans un ordre aléatoire.
            L'ordre aléatoire est défini en fonction de la pioche des joueurs passés
            en paramètres, celui qui aura pioché la main avec le plus de tuiles qui
            ont un élément commun commençera et cela se suivra alors de façon logique.
            Il initie le board avec des tuiles nulles afin d'avoir une liste pleine
            afin de gérer cela plus facilement depuis le vb.
            Il n'initie pas placed_tiles qui reste à sa valeur initiale.
            Le constructeur peut également renvoyer une exception de type
            WrongNumberOfPlayerException si jamais le nombre de joueurs au sein de
            la liste passé en arguments n'est pas conforme au règles du Qwirkle
            (à savoir entre 2 et 4 joueurs).
            */
            if (players.Count < 2 || players.Count > 4)
            {
                throw new WrongNumberOfPlayerException((short)players.Count);
            }
            for (short i = 0; i < 3; i++)
            {
                for (short j = 0; j < 6; j++)
                {
                    for (short k = 0; k < 6; k++)
                    {
                        Tile tile = new Tile(i, j, k);
                        this.stack.Add(tile);
                    }
                }
            }
            /*
            On fait piocher les joueurs et on en enregistre le maximum
            de points communs entre ses tuiles.
            */
            this.players.AddRange(players);
            short[] prio = new short[players.Count];
            short[] value = new short[6];
            for (short i = 0; i < 6; i++)
            {
                value[i] = 0;
            }
            for (short i = 0; i < players.Count; i++)
            {
                players[i].Draw(this);
                short[] comptc = new short[7];
                short[] comptf = new short[7];
                for (short j = 0; j < 7; j++)
                {
                    comptc[j] = 0;
                    comptf[j] = 0;
                }
                for (short j = 0; j < players[i].getRack().Count; j++)
                {
                    comptc[players[i].getRack()[j].getColor()]++;
                    comptf[players[i].getRack()[j].getShape()]++;
                }
                for (short j = 0; j < 6; j++)
                {
                    if (comptc[6] < comptc[j])
                    {
                        comptc[6] = comptc[j];
                    }
                    if (comptf[6] < comptf[j])
                    {
                        comptf[6] = comptf[j];
                    }
                }
                if (comptc[6] > comptf[6])
                {
                    prio[i] = comptc[6];
                    value[comptc[6] - 1]++;
                }
                else
                {
                    prio[i] = comptf[6];
                    value[comptf[6] - 1]++;
                }
            }
            /*
            Une fois fait on retire les joueurs qu'on avait ajouté
            afin qu'il ai le droit de piocher, afin de pouvoir les
            reagencer.
            */
            for (short i = 0; i < players.Count; i++)
            {
                this.players.Remove(players[i]);
            }
            for (short i = 0; i < 6; i++)
            {
                /*
                Cas où il y a plusieurs joueurs avec la même valeur
                de priorité.
                */
                if (value[i] > 1)
                {
                    /*
                    On compare donc les âges de ces joueurs pour savoir si
                    certains on le même âge.
                    */
                    List<Player> LTJTemp = new List<Player>(value[i]);
                    List<short> Age = new List<short>(value[i]);
                    List<short> AgeDiff = new List<short>(value[i]);
                    short comptage = 0;
                    for (short j = 0; j < players.Count; j++)
                    {
                        if (prio[j] == i + 1)
                        {
                            LTJTemp.Add(players[j]);
                            Age.Add(players[j].getAge());
                            if (AgeDiff.Contains(players[j].getAge()) == false)
                            {
                                AgeDiff.Add(players[j].getAge());
                                comptage++;
                            }
                        }
                    }
                    /*
                    Cas où certains ont le même âge justement et où donc
                    on les place à la place que leur réserve leur âge mais
                    dans un ordre aléatoire entre les personnes possèdant le
                    même âge.
                    */
                    if (comptage != value[i])
                    {
                        Random R = new Random();
                        short[] age = new short[comptage];
                        short[] agecompt = new short[comptage];
                        for (short j = 0; j < comptage; j++)
                        {
                            age[j] = 0;
                            agecompt[j] = 0;
                        }
                        for (short j = 0; j < value[i]; j++)
                        {
                            age[AgeDiff.IndexOf(LTJTemp[j].getAge())]++;
                        }
                        for (short j = 0; j < LTJTemp.Count; j++)
                        {
                            if (j != (value[i] - 1))
                            {
                                if (age[AgeDiff.IndexOf(players[j].getAge())] > 1)
                                {
                                    int min = 0;
                                    int max = 0;
                                    for (short l = 0; l < this.players.Count; l++)
                                    {
                                        if (LTJTemp[j].getAge() == this.players[l].getAge())
                                        {
                                            min = l;
                                            max = l + agecompt[AgeDiff.IndexOf(players[j].getAge())];
                                        }
                                    }
                                    if (min != max)
                                    {
                                        short k = (short)R.Next(min, max);
                                        this.players.Insert(k, LTJTemp[j]);
                                        LTJTemp.Remove(LTJTemp[j]);
                                    } else
                                    {
                                        this.players.Insert(min, LTJTemp[j]);
                                        LTJTemp.Remove(LTJTemp[min]);
                                    }
                                    agecompt[AgeDiff.IndexOf(players[j].getAge())]++;
                                    j--;
                                } else
                                {
                                    bool test = false;
                                    short k = j;
                                    while (k < LTJTemp.Count && test == false)
                                    {
                                        if (players[j].getAge() < this.players[k].getAge())
                                        {
                                            this.players.Insert(k, LTJTemp[j]);
                                            LTJTemp.Remove(LTJTemp[j]);
                                            j--;
                                            test = true;
                                        }
                                        k++;
                                    }
                                    if (test == false)
                                    {
                                        this.players.Add(LTJTemp[j]);
                                        LTJTemp.Remove(LTJTemp[j]);
                                        j--;
                                    }
                                }
                            }
                            else
                            {
                                this.players.Add(LTJTemp[j]);
                            }
                        }
                    }
                    /*
                    Cas où tous ont un âge différent, donc on ne fait que trier.
                    */
                    else
                    {
                        for (short j = 0; j < value[i]; j++)
                        {
                            if (j != (value[i] - 1))
                            {
                                bool test = false;
                                for (short k = 0; k < this.players.Count; k++)
                                {
                                    if (players[j].getAge() < this.players[k].getAge())
                                    {
                                        this.players.Insert(k, LTJTemp[j]);
                                        test = true;
                                    }
                                }
                                if (test == false)
                                {
                                    this.players.Add(LTJTemp[j]);
                                }
                            } else
                            {
                                this.players.Add(LTJTemp[j]);
                            }
                        }
                    }
                }
                /*
                Cas où on a qu'un joueur pour la valeur de priorité.
                */
                else if (value[i] == 1)
                {
                    for (short j = 0; j < players.Count; j++)
                    {
                        if (prio[j] == i + 1)
                        {
                            this.players.Add(players[j]);
                        }
                    }
                }
            }
            this.boardsizex = 400;
            this.boardsizey = 400;
            this.tilesizex = 40;
            this.tilesizey = 40;
            this.nbcolumndisplay = 10;
            this.nblinedisplay = 10;
            this.nbcolumn = 1;
            this.nbline = 1;
            this.board = new List<List<Tile>>(this.nbline);
            for (short i = 0; i < this.nbline; i++)
            {
                List<Tile> Line = new List<Tile>(this.nbcolumn);
                this.board.Add(Line);
                for (short f = 0; f < this.nbcolumn; f++)
                {
                    Tile tilenull = new Tile();
                    this.board[i].Add(tilenull);
                }
            }
        }
        public List<Tile> getStack()
        {
            /*
            Accesseur de la liste de tuiles stack.
            */
            return this.stack;
        }
        public short getNbColumn()
        {
            /*
            Accesseur du short nbcolumn.
            */
            return this.nbcolumn;
        }
        public short getNbLine()
        {
            /*
            Accesseur du short nbline.
            */
            return this.nbline;
        }
        public short getNbColumnDisplay()
        {
            /*
            Accesseur du short nbcolumndisplay.
            */
            return this.nbcolumndisplay;
        }
        public short getNbLineDisplay()
        {
            /*
            Accesseur du short nblinedisplay.
            */
            return this.nblinedisplay;
        }
        public List<List<Tile>> getBoard()
        {
            /*
            Accesseur de la liste bidimensionnelle de tuiles board.
            */
            return this.board;
        }
        public short getBoardSizeX()
        {
            /*
            Accesseur du short boardsizex.
            */
            return this.boardsizex;
        }
        public short getBoardSizeY()
        {
            /*
            Accesseur du short boardsizey.
            */
            return this.boardsizey;
        }
        public short getTileSizeX()
        {
            /*
            Accesseur du short boardtilex.
            */
            return this.tilesizex;
        }
        public short getTileSizeY()
        {
            /*
            Accesseur du short boardtiley.
            */
            return this.tilesizey;
        }
        public List<Tile> getPlaced_Tiles()
        {
            /*
            Accesseur de la liste de tuiles placed_tiles.
            */
            return this.placed_tiles;
        }
        public List<Player> getPlayers()
        {
            /*
            Accesseur de la liste de player players.
            */
            return this.players;
        }
        public void setNbColumn(short nb)
        {
            /*
            Redéfinition de la valeur du short nbcolumn.
            Et redimensionnement du plateau en conséquence.
            Si le plateau est non vide alors la méthode renverra
            une exception de type BaordIsNotEmpty().
            */
            if (this.FirstTile() == true)
            {
                this.nbcolumn = nb;
                this.board.Clear();
                this.board = new List<List<Tile>>(this.nbline);
                for (short i = 0; i < this.nbline; i++)
                {
                    List<Tile> Line = new List<Tile>(this.nbcolumn);
                    this.board.Add(Line);
                    for (short f = 0; f < this.nbcolumn; f++)
                    {
                        Tile tilenull = new Tile();
                        this.board[i].Add(tilenull);
                    }
                }
            } else
            {
                throw new BoardIsNotEmptyException();
            }
        }
        public void setNbLine(short nb)
        {
            /*
            Redéfinition de la valeur du short nbline.
            Et redimensionnement du plateau en conséquence.
            Si le plateau est non vide alors la méthode renverra
            une exception de type BaordIsNotEmpty().
            */
            if (this.FirstTile() == true)
            {
                this.nbline = nb;
                this.board.Clear();
                this.board = new List<List<Tile>>(this.nbline);
                for (short i = 0; i < this.nbline; i++)
                {
                    List<Tile> Line = new List<Tile>(this.nbcolumn);
                    this.board.Add(Line);
                    for (short f = 0; f < this.nbcolumn; f++)
                    {
                        Tile tilenull = new Tile();
                        this.board[i].Add(tilenull);
                    }
                }
            }
            else
            {
                throw new BoardIsNotEmptyException();
            }
        }
        public void setNbColumnDisplay(short nb)
        {
            /*
            Redéfinition de la valeur du short nbcolumndisplay.
            Cette méthode redéfinie la taille des tuiles en cas
            de besoin.
            */
            this.nbcolumndisplay = nb;
            this.tilesizey = (short)(this.boardsizey / this.nbcolumndisplay);
        }
        public void setNbLineDisplay(short nb)
        {
            /*
            Redéfinition de la valeur du short nbcolumndisplay.
            Cette méthode redéfinie la taille des tuiles en cas
            de besoin.
            */
            this.nblinedisplay = nb;
            this.tilesizex = (short)(this.boardsizex / this.nblinedisplay);
        }
        public void setBoardSizeX(short size)
        {
            /*
            Redéfinition de la valeur du short boardsizex.
            Cette méthode redéfinie la taille des tuiles en cas
            de besoin.
            */
            this.boardsizex = size;
            this.tilesizex = (short)(this.boardsizex / this.nblinedisplay);
        }
        public void setBoardSizeY(short size)
        {
            /*
            Redéfinition de la valeur du short boardsizey.
            Cette méthode redéfinie la taille des tuiles en cas
            de besoin.
            */
            this.boardsizey = size;
            this.tilesizey = (short)(this.boardsizey / this.nbcolumndisplay);
        }
        public void AddBoard(Tile tile, short x, short y)
        {
            /*
            Methode AddBoard().
            Ajoute la tuile passée en argument au Board aux
            coordonées x et y passées en arguments.
            Cette méthode agrandit aussi en cas de besoin le
            board et renvoie une Exception indiquant "Mauvaise
            Position pour la Tuile" si jamais on tente de place
            la tuile sur une tuile non nulle déjà présente au
            sein du board.
            La méthode peut également renvoyer un Exception
            indiquant "Tuile en dehors des limites du board"
            si on passe en paramètres des valeurs en dehors
            du board.
            */
            Tile tilenull = new Tile();
            if (x >= 0 && x < this.nbline && y >= 0 && y < this.nbcolumn)
            {
                if (this.board[x][y] == tilenull)
                {
                    if (x == 0)
                    {
                        this.nbline++;
                        List<List<Tile>> boardt = new List<List<Tile>>(this.nbline);
                        List<Tile> Line = new List<Tile>(this.nbcolumn);
                        boardt.Add(Line);
                        for (short f = 0; f < this.nbcolumn; f++)
                        {
                            boardt[0].Add(tilenull);
                        }
                        boardt.AddRange(this.board);
                        this.board.Clear();
                        this.board.AddRange(boardt);
                        x++;
                    }
                    if (x == this.nbline - 1)
                    {
                        this.nbline++;
                        List<Tile> Line = new List<Tile>(this.nbcolumn);
                        for (short i = 0; i < this.nbcolumn; i++)
                        {
                            Line.Add(tilenull);
                        }
                        this.board.Add(Line);
                    }
                    if (y == 0)
                    {
                        this.nbcolumn++;
                        for (short f = 0; f < this.nbline; f++)
                        {
                            this.board[f].Insert(0, tilenull);
                        }
                        y++;
                    }
                    if (y == this.nbcolumn - 1)
                    {
                        this.nbcolumn++;
                        for (short f = 0; f < this.nbline; f++)
                        {
                            this.board[f].Insert(this.nbcolumn - 1, tilenull);
                        }
                    }
                    this.board[x][y] = tile;
                }
                else
                {
                    throw new WrongPositionForTileException("This place is already occupied.", this, tile, x, y);
                }
            }
            else
            {
                throw new TileOutOfLimitsException();
            }
        }
        public void AddTile(Tile tile, short x, short y)
        {
            /*
            Methode AddTile().
            Cette méthode appelle la méthode AddBoard() et
            si elle ne rencontre pas d’exception elle ajoute
            la tuile au buffer placed_tile.
            */
            AddBoard(tile, x, y);
            this.placed_tiles.Add(tile);
        }
        public void AddStack(Tile tile)
        {
            /*
            Methode AddStack().
            Ajoute la tuile passée en argument au Stack.
            */
            this.stack.Add(tile);
        }
        public void RemoveStack(Tile tile)
        {
            /*
            Methode RemoveStack().
            Retire la tuile passée en argument au Stack après avoir vérifié
            sa présence au sein de celui-ci.
            Si cela n'est pas le cas, une Exception est retournée affirmant
            "Suppression d'une mauvaise tuile".
            */
            if (this.stack.Contains(tile))
            {
                this.stack.Remove(tile);
            }
            else
            {
                throw new RemoveWrongTilesException();
            }
        }
        public bool Is_Stack_Empty()
        {
            /*
            Methode Is_Stack_Empty().
            Compte le nombre de tuiles au sein du stack et returne true
            si celui-ci est vide.
            */
            if (stack.Count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool Player_Exist(Player player)
        {
            /*
            Methode Player_Exist().
            Cette méthode affirme ou non la présence du joueur
            passé en paramètre au sein de la partie symbolisée
            par l'entité Board par laquelle elle est appelée.
            */
            for (short i = 0; i < this.players.Count; i++)
            {
                if (player.Equals(players[i]))
                {
                    return true;
                }
            }
            return false;
        }
        public int[] Board_Search(Tile tile)
        {
            /*
            Methode Board_Search().
            Cette méthode recherche la tuile passée en paramètre
            et renvoie ses coordonnées, si la tuile est trouvé
            dans le cas contraire, la méthode renvoie une Exception
            de type TileDoes'ntExistException.
            */
            for (short i = 0; i < this.nbline; i++)
            {
                for (short j = 0; j < this.nbcolumn; j++)
                {
                    if (this.board[i][j] == tile)
                    {
                        int[] coord = new int[2];
                        coord[0] = i;
                        coord[1] = j;
                        return coord;
                    }
                }
            }
            throw new TileDoes_ntExistException();
        }
        public bool FirstTile()
        {
            /*
            Methode FirstTile().
            Cette méthode retourne true si il n'y a encore
            aucune tuile posé et false, s'il y en a au moins
            une de posée.
            */
            Tile tilenull = new Tile();
            for (short i = 0; i < this.nbline; i++)
            {
                for (short j = 0; j < this.nbcolumn; j++)
                {
                    if (this.board[i][j] != tilenull)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public short NbTile()
        {
            /*
            Methode NbTile().
            Cette méthode renvoie le nombre de tuile
            posée au sein du board.
            */
            short result = 0;
            Tile tilenull = new Tile();
            for (short i = 0; i < this.nbline; i++)
            {
                for (short j = 0; j < this.nbcolumn; j++)
                {
                    if (this.board[i][j] != tilenull)
                    {
                        result++;
                    }
                }
            }
            return result;
        }
        public bool Rules_Verification(Tile tile, short x_tile, short y_tile)
        {
            /*
            Methode Rules_Verification().
            */
            short x = x_tile;
            short y = y_tile;
            int alignement_verification = 0;
            short left_compt = 0;
            short right_compt = 0;
            short down_compt = 0;
            short up_compt = 0;
            Tile tilenull = new Tile();       //LES TILES NULLS

            // verify the tile is in the board
            if (x < 0 || x > this.nbline - 1 || y < 0 || y > this.nbcolumn - 1)
                throw new TileOutOfLimitsException();

            //verify the place isn't already occupied
            if (board[x][y] != tilenull)
                throw new WrongPositionForTileException("This place is already occupied.", this, tile, x_tile, y_tile);
            if (this.FirstTile() == true)
            {
                if (this.nbcolumn % 2 == 1)
                {
                    if (this.nbline % 2 == 1)
                    {
                        //verify the center of the board isn't empty
                        if (board[(this.nbline - 1) / 2][(this.nbcolumn - 1) / 2] == tilenull && (x != ((this.nbline - 1) / 2) || y != (this.nbcolumn - 1) / 2))
                            throw new WrongPositionForTileException("Center of the Board is Empty.", this, tile, x_tile, y_tile);
                    }
                    else
                    {
                        //verify the center of the board isn't empty
                        if ((board[this.nbline / 2][(this.nbcolumn - 1) / 2] == tilenull && (x != this.nbline / 2 || y != (this.nbcolumn - 1) / 2)) || (board[(this.nbline / 2) - 1][(this.nbcolumn - 1) / 2] == tilenull && (x != (this.nbline / 2) - 1 || y != (this.nbcolumn - 1) / 2)))
                            throw new WrongPositionForTileException("Center of the Board is Empty.", this, tile, x_tile, y_tile);
                    }
                }
                else
                {
                    if (this.nbline % 2 == 1)
                    {
                        //verify the center of the board isn't empty
                        if ((board[(this.nbline - 1) / 2][this.nbcolumn / 2] == tilenull && (x != (this.nbline - 1) / 2 || y != this.nbcolumn / 2)) || (board[(this.nbline - 1) / 2][(this.nbcolumn / 2) - 1] == tilenull && (x != (this.nbline - 1) / 2 || y != (this.nbcolumn / 2) - 1)))
                            throw new WrongPositionForTileException("Center of the Board is Empty.", this, tile, x_tile, y_tile);
                    }
                    else
                    {
                        //verify the center of the board isn't empty
                        if ((board[this.nbline / 2][this.nbcolumn / 2] == tilenull && (x != this.nbline / 2 || y != this.nbcolumn / 2)) || (board[(this.nbline / 2) - 1][(this.nbcolumn / 2) - 1] == tilenull && (x != (this.nbline / 2) - 1 || y != (this.nbcolumn / 2) - 1)))
                            throw new WrongPositionForTileException("Center of the Board is Empty.", this, tile, x_tile, y_tile);
                    }
                }
            }
            else
            {
                if (x > 0 && x < this.nbline - 1)
                {
                    if (y > 0 && y < this.nbcolumn - 1)
                    {
                        //Verify that the placed tuile isn't alone
                        if (board[x][y + 1] == tilenull && board[x][y - 1] == tilenull && board[x + 1][y] == tilenull && board[x - 1][y] == tilenull)
                            throw new WrongPositionForTileException("Tile is Alone.", this, tile, x_tile, y_tile);
                    }
                    else if (y == 0)
                    {
                        //Verify that the placed tuile isn't alone
                        if (board[x][y + 1] == tilenull && board[x + 1][y] == tilenull && board[x - 1][y] == tilenull)
                            throw new WrongPositionForTileException("Tile is Alone.", this, tile, x_tile, y_tile);
                    }
                    else if (y == this.nbcolumn - 1)
                    {
                        //Verify that the placed tuile isn't alone
                        if (board[x][y - 1] == tilenull && board[x + 1][y] == tilenull && board[x - 1][y] == tilenull)
                            throw new WrongPositionForTileException("Tile is Alone.", this, tile, x_tile, y_tile);
                    }
                }
                else if (x == 0)
                {
                    if (y > 0 && y < this.nbcolumn - 1)
                    {
                        //Verify that the placed tuile isn't alone
                        if (board[x][y + 1] == tilenull && board[x][y - 1] == tilenull && board[x + 1][y] == tilenull)
                            throw new WrongPositionForTileException("Tile is Alone.", this, tile, x_tile, y_tile);
                    }
                    else if (y == 0)
                    {
                        //Verify that the placed tuile isn't alone
                        if (board[x][y + 1] == tilenull && board[x + 1][y] == tilenull)
                            throw new WrongPositionForTileException("Tile is Alone.", this, tile, x_tile, y_tile);
                    }
                    else if (y == this.nbcolumn - 1)
                    {
                        //Verify that the placed tuile isn't alone
                        if (board[x][y - 1] == tilenull && board[x + 1][y] == tilenull)
                            throw new WrongPositionForTileException("Tile is Alone.", this, tile, x_tile, y_tile);
                    }
                }
                else if (x == this.nbline - 1)
                {
                    if (y > 0 && y < this.nbcolumn - 1)
                    {
                        //Verify that the placed tuile isn't alone
                        if (board[x][y + 1] == tilenull && board[x][y - 1] == tilenull && board[x - 1][y] == tilenull && (board[this.nbline / 2][this.nbcolumn / 2] != tilenull || board[(this.nbline / 2) - 1][(this.nbcolumn / 2) - 1] != tilenull))
                            throw new WrongPositionForTileException("Tile is Alone.", this, tile, x_tile, y_tile);
                    }
                    else if (y == 0)
                    {
                        //Verify that the placed tuile isn't alone
                        if (board[x][y + 1] == tilenull && board[x - 1][y] == tilenull)
                            throw new WrongPositionForTileException("Tile is Alone.", this, tile, x_tile, y_tile);
                    }
                    else if (y == this.nbcolumn - 1)
                    {
                        //Verify that the placed tuile isn't alone
                        if (board[x][y - 1] == tilenull && board[x - 1][y] == tilenull)
                            throw new WrongPositionForTileException("Tile is Alone.", this, tile, x_tile, y_tile);
                    }
                }
                if (x > 0)
                {
                    x--;
                    while (x > 0 && board[x][y] != tilenull)    //Verify that the left tuiles have only 1 similarity with the placed tuile
                    {
                        if (!(tile.Is_similar(board[x][y])))
                            throw new WrongPositionForTileException("The left tiles don't have only 1 similarity with this tile.", this, tile, x_tile, y_tile);
                        for (int index = 0; index < this.placed_tiles.Count; index++)
                        {
                            if (board[x][y] == placed_tiles[index])
                                alignement_verification++;  //traveling the aligned tiles to the one u wanna place, we verify it correpsond to an already placed one
                        }
                        left_compt++;
                        x--;
                    }
                }
                x = (short) (x_tile + 1);
                if (x < this.nbline - 1)
                {
                    while (x < (this.nbline - 1) && board[x][y] != tilenull)  //right
                    {
                        if (!(tile.Is_similar(board[x][y])))
                            throw new WrongPositionForTileException("The right tile don't have only 1 similarity with this tile.", this, tile, x_tile, y_tile);
                        for (int index = 0; index < this.placed_tiles.Count; index++)
                        {
                            if (board[x][y] == placed_tiles[index])
                                alignement_verification++;
                        }
                        right_compt++;
                        x++;
                    }
                }
                x = x_tile;
                if (y > 0)
                {
                    y = (short) (y_tile - 1);
                    while (y > 0 && board[x][y] != tilenull)
                    {
                        if (!(tile.Is_similar(board[x][y]))) //up
                            throw new WrongPositionForTileException("The up tiles don't have only 1 similarity with this tile.", this, tile, x_tile, y_tile);
                        for (int index = 0; index < this.placed_tiles.Count; index++)
                        {
                            if (board[x][y] == placed_tiles[index])
                                alignement_verification++;
                        }
                        up_compt++;
                        y--;
                    }
                }
                y = (short) (y_tile + 1);
                if (y < this.nbcolumn - 1)
                {
                    while (y < (this.nbcolumn - 1) && board[x][y] != tilenull)
                    {
                        if (!(tile.Is_similar(board[x][y]))) //down
                            throw new WrongPositionForTileException("The down tiles don't have only 1 similarity with this tile.", this, tile, x_tile, y_tile);
                        for (int index = 0; index < this.placed_tiles.Count; index++)
                        {
                            if (board[x][y] == placed_tiles[index])
                                alignement_verification++;
                        }
                        down_compt++;
                        y++;
                    }
                }
                if (alignement_verification != this.placed_tiles.Count) //if all the placed tiles aren't found traveling all the aligned ones to the one u wanna place, then they aren't aligned
                    throw new WrongPositionForTileException("This tile aren't aligned with other tile place in this turn.", this, tile, x_tile, y_tile);
                if (right_compt > 0)
                {
                    for (int right = 1; right <= right_compt; right++)
                    {
                        for (int left = 1; left <= left_compt; left++)
                        {
                            if (!(board[x_tile + right][y_tile].Is_similar(board[x_tile - left][y_tile])))
                                throw new WrongPositionForTileException("The tile in this place create a wrong word.", this, tile, x_tile, y_tile);
                        }
                    }
                }
                if (up_compt > 0)
                {
                    for (int up = 1; up <= up_compt; up++)
                    {
                        for (int down = 1; down <= down_compt; down++)
                        {
                            if (!(board[x_tile][y_tile - up].Is_similar(board[x_tile][y_tile + down])))
                                throw new WrongPositionForTileException("The tile in this place create a wrong word.", this, tile, x_tile, y_tile);
                        }
                    }
                }
            }
            return true;
        }
        public short Calcul_Score()
        {
            /*
            Methode Calcul_Score().
            Cette méthode permet de calculer l'incrémentation du
            score du Joueur en cours, selon les règles du Qwirkle.
            */
            int[] coord = new int[2];
            short score = 0;
            short points = 0;
            int x, y;
            Tile tilenull = new Tile();       //LES TILES NULLS
            if (this.placed_tiles.Count > 0)
            {
                coord = this.Board_Search(this.placed_tiles[0]);
                x = coord[0];
                y = coord[1];
                if (this.placed_tiles.Count >= 2)
                {

                    if (y - Board_Search(this.placed_tiles[1])[1] == 0)
                    { //CAS ou les tuiles sont alignés verticalement
                        for (int index = 0; index < placed_tiles.Count; index++) //counting everypoints all the tiles generate individually
                        {
                            points = 0;
                            coord = this.Board_Search(this.placed_tiles[index]);
                            x = coord[0];
                            y = coord[1];
                            while (board[x][y] != tilenull)
                            {
                                y++;
                            }
                            y--;
                            while (board[x][y] != tilenull)
                            {
                                points++;
                                y--;
                            }
                            if (points >= 6)
                                points += 6;
                            if (points > 1)
                                score += points;
                        }
                        points = 0;
                        coord = this.Board_Search(this.placed_tiles[0]);
                        x = coord[0];
                        y = coord[1];
                        while (board[x][y] != tilenull)
                            x--;
                        x++;
                        while (board[x][y] != tilenull)
                        {
                            points++;
                            x++;
                        }
                        if (points >= 6)
                            points += 6;
                        score += points;

                    }
                    else //CAS ou les tuiles sont allignées horizontalement
                    {
                        for (int index = 0; index < placed_tiles.Count; index++) //counting everypoints all the tiles generate individually
                        {
                            points = 0;
                            coord = this.Board_Search(this.placed_tiles[index]);
                            x = coord[0];
                            y = coord[1];
                            while (board[x][y] != tilenull)
                            {
                                x++;
                            }
                            x--;
                            while (board[x][y] != tilenull)
                            {
                                points++;
                                x--;
                            }
                            if (points >= 6)
                                points += 6;
                            if (points > 1)
                                score += points;
                        }
                        points = 0;
                        coord = this.Board_Search(this.placed_tiles[0]);
                        x = coord[0];
                        y = coord[1];
                        while (board[x][y] != tilenull)
                            y--;
                        y++;
                        while (board[x][y] != tilenull)
                        {
                            points++;
                            y++;
                        }
                        if (points >= 6)
                            points += 6;
                        score += points;
                    }
                }
                else //CAS ou il n'y a qu'une tuile
                {
                    if (this.NbTile() == 1)
                    {
                        score++;
                    }
                    else
                    {
                        while (board[x][y] != tilenull)
                            x++;
                        x--;
                        while (board[x][y] != tilenull)
                        {
                            points++;
                            x--;
                        }
                        if (points >= 6)
                            points += 6;
                        if (points > 1)
                            score += points;
                        points = 0;
                        x = coord[0];
                        while (board[x][y] != tilenull)
                        {
                            y++;
                        }
                        y--;
                        while (board[x][y] != tilenull)
                        {
                            points++;
                            y--;
                        }
                        if (points >= 6)
                            points += 6;
                        if (points > 1)
                            score += points;
                    }
                }
                return score;
            } else
            {
                throw new NoTilePlacedInThisTurnException();
            }
        }
        public bool PossibilityOfPlaying(List<Tile> tiles)
        {
            /*
            Methode PossibilityOfPlaying().
            Cette méthode renvoie true si on peut jouer
            ou false, si à contrario on ne peut pas
            avec les paramètres passées en arguments.
            Elle a besoin d'une liste de tuiles à tester
            et teste alors cette liste sur le board depuis
            lequel elle a été apellée.
            */
            Tile tilenull = new Tile();
            for (short o = 0; o < tiles.Count; o++)
            {
                for (short m = 0; m < nbline; m++)
                {
                    for (short n = 0; n < nbcolumn; n++)
                    {
                        try
                        {
                            if (this.Rules_Verification(tiles[o], m, n))
                            {
                                return true;
                            }
                        }
                        catch (WrongPositionForTileException)
                        {

                        }
                    }
                }
            }
            return false;
        }
        public void ResizeBoard()
        {
            /*
            Méthode ResizeBoard().
            Cette méthode redimensionne au besoin le board afin
            de palier à toute utilisation de mémoire excessive
            et inutile.
            */
            bool test = false;
            Tile tilenull = new Tile();
            List<Tile> LNULL = new List<Tile>();
            for (short j = 0; j < this.nbcolumn; j++)
            {
                LNULL.Add(tilenull);
            }
            for (short i = 0; i < this.nbline; i++)
            {
                bool Test = true;
                for (short j = 0; j < this.nbcolumn; j++)
                {
                    if (!(LNULL[j] == this.board[i][j]))
                    {
                        Test = false;
                    }
                }
                if (Test == true)
                {
                    if (test == false)
                    {
                        test = true;
                    }
                    else
                    {
                        this.board.RemoveAt(i);
                        i--;
                        this.nbline--;
                    }
                } else
                {
                    if (test == true)
                    {
                        test = false;
                    }
                }
            }
            test = false;
            LNULL.Clear();
            for (short j = 0; j < this.nbline; j++)
            {
                LNULL.Add(tilenull);
            }
            for (short i = 0; i < this.nbcolumn; i++)
            {
                bool Test = true;
                for (short j = 0; j < this.nbline; j++)
                {
                    if (!(LNULL[j] == this.board[j][i]))
                    {
                        Test = false;
                    }
                }
                if (Test == true)
                {
                    if (test == false)
                    {
                        test = true;
                    }
                    else
                    {
                        for (short k = 0; k < this.nbline; k++)
                        {
                            this.board[k].RemoveAt(i);
                        }
                        i--;
                        this.nbcolumn--;
                    }
                }
                else
                {
                    if (test == true)
                    {
                        test = false;
                    }
                }
            }
        }
        public void CancelPlacement()
        {
            /*
            Méthode CancelPlacement().
            Cette méthode annule les placement du tour en cours.
            Pour chaque tuile qui a été posé ce tour-ci elle est
            enlevée du board et de placed_tiles avant d'être
            remise dans le rack du joueur en cours.
            */
            if (this.placed_tiles.Count > 0)
            {
                Tile tilenull = new Tile();
                short comptmax = (short) this.placed_tiles.Count;
                for (short i = 0; i <  comptmax; i++)
                {
                    int[] coord = Board_Search(this.placed_tiles[comptmax - (i + 1)]);
                    this.board[coord[0]][coord[1]] = tilenull;
                    this.players[0].AddRack(this.placed_tiles[comptmax - (i + 1)]);
                    this.placed_tiles.Remove(this.placed_tiles[comptmax - (i + 1)]);
                }
                this.ResizeBoard();
            } else
            {
                throw new NoTilePlacedInThisTurnException();
            }

        }
        public int EndTurn()
        {
            /*
            Methode EndTurn().
            Cette méthode appelée en fin de tour achève le tour
            du joueur en cours, si la pioche est vide et que
            parmis tout les joueurs aucune autre pose de tuile
            n'est possible, la partie s'achève.
            Cette méthode renvoie 0 si la partie se poursuit, 1 si
            elle se poursuit et qu'un joueur n'a pas pu piocher car
            le sac est vide et 2 si la partie s'achève.
            */
            List<Player> player = new List<Player>(this.players.Count - 1);
            for (short i = 1; i < this.players.Count; i++)
            {
                player.Add(players[i]);
            }
            Player P_actuel = this.players[0];
            this.players.Clear();
            this.players.AddRange(player);
            this.players.Add(P_actuel);
            if (this.placed_tiles.Count > 0)
            {
                this.players[this.players.Count - 1].Increase_score(this.Calcul_Score());
                this.placed_tiles.Clear();
                try
                {
                    this.players[this.players.Count - 1].Draw(this);
                }
                catch (StackIsEmptyException)
                {
                    List<Tile> tiles = new List<Tile>();
                    for (short l = 0; l < players.Count; l++)
                    {
                        for (short k = 0; k < players[l].getRack().Count; k++)
                        {
                            tiles.Add(players[l].getRack()[k]);
                        }
                    }
                    if (this.PossibilityOfPlaying(tiles) == true)
                    {
                        return 1;
                    }
                    else
                    {
                        this.players[this.players.Count - 1].Increase_score(6);
                        return 2;
                    }
                }
                return 0;
            }
            else
            {
                List<Tile> tiles = new List<Tile>();
                for (short l = 0; l < players.Count; l++)
                {
                    for (short k = 0; k < players[l].getRack().Count; k++)
                    {
                        tiles.Add(players[l].getRack()[k]);
                    }
                }
                tiles.AddRange(this.stack);
                if (this.PossibilityOfPlaying(tiles) == true)
                {
                    return 0;
                }
                else
                {
                    this.players[this.players.Count - 2].Increase_score(6);
                    return 2;
                }
            }
        }
    }
}
