﻿Public Class Settings
    Dim nblinedisplay As Short = Game.GetBoard().getNbLine()
    Dim nbcolumndisplay As Short = Game.GetBoard().getNbColumnDisplay()
    Dim boardsizex As Short = Game.GetBoard().getBoardSizeX()
    Dim boardsizey As Short = Game.GetBoard().getBoardSizeY()
    Private Sub btn_return_Click() Handles btn_return.Click
        'Retour au menu Contextuel.
        Menu_Context.Enabled = True
        Game.ReloadBoard()
        Me.Hide()
    End Sub
    Private Sub numupdown_ValueChanged(sender As Object, e As EventArgs) Handles numupdown_boardsizey.ValueChanged, numupdown_boardsizex.ValueChanged, numupdown_nbcolumndisplay.ValueChanged, numupdown_nblinedisplay.ValueChanged
        'En cas de modification des NumericUpDown on
        'modifie la valeur associé au NumericUpDown.
        Dim numupdown As NumericUpDown = sender
        If numupdown.Name.Split("_")(1) = "nblinedisplay" Then
            nblinedisplay = numupdown.Value
        ElseIf numupdown.Name.Split("_")(1) = "nbcolumndisplay" Then
            nbcolumndisplay = numupdown.Value
        ElseIf numupdown.Name.Split("_")(1) = "boardsizex" Then
            boardsizex = numupdown.Value
        ElseIf numupdown.Name.Split("_")(1) = "boardsizey" Then
            boardsizey = numupdown.Value
        End If
        If Game.GetBoard().getBoardSizeX() = boardsizex And Game.GetBoard().getBoardSizeY() = boardsizey And Game.GetBoard().getNbColumnDisplay() = nbcolumndisplay And Game.GetBoard().getNbLineDisplay() = nbcolumndisplay Then
            btn_return.Text = "Return"
        Else
            btn_return.Text = "Return (And Save)"
        End If
    End Sub
    Public Function GetNbLineDisplay() As Short
        'Accesseur de NbLine'
        Return nblinedisplay
    End Function
    Public Function GetNbColumnDisplay() As Short
        'Accesseur de NbColumn'
        Return nbcolumndisplay
    End Function
    Public Function GetBoardSizeX() As Short
        'Accesseur du BoardSizeY'
        Return boardsizex
    End Function
    Public Function GetBoardSizeY() As Short
        'Accesseur du BoardSizeY'
        Return boardsizey
    End Function
    Public Sub MàJ()
        nblinedisplay = Game.GetBoard().getNbLineDisplay()
        nbcolumndisplay = Game.GetBoard().getNbColumnDisplay()
        boardsizex = Game.GetBoard().getBoardSizeX()
        boardsizey = Game.GetBoard().getBoardSizeY()
        numupdown_nblinedisplay.Value = nblinedisplay
        numupdown_nbcolumndisplay.Value = nbcolumndisplay
        numupdown_boardsizex.Value = boardsizex
        numupdown_boardsizey.Value = boardsizey
    End Sub
End Class