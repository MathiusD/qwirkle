﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Game
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.pnl_board = New System.Windows.Forms.Panel()
        Me.pnl_rack = New System.Windows.Forms.Panel()
        Me.btn_end = New System.Windows.Forms.Button()
        Me.btn_trade = New System.Windows.Forms.Button()
        Me.btn_i_am = New System.Windows.Forms.Button()
        Me.pnl_actplayer = New System.Windows.Forms.Panel()
        Me.pnl_players = New System.Windows.Forms.Panel()
        Me.btn_select = New System.Windows.Forms.Button()
        Me.btn_load_board = New System.Windows.Forms.Button()
        Me.lbl_stack = New System.Windows.Forms.Label()
        Me.btn_menu = New System.Windows.Forms.Button()
        Me.btn_cancelplacement = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'pnl_board
        '
        Me.pnl_board.AutoScroll = True
        Me.pnl_board.Location = New System.Drawing.Point(375, 10)
        Me.pnl_board.Name = "pnl_board"
        Me.pnl_board.Size = New System.Drawing.Size(410, 410)
        Me.pnl_board.TabIndex = 0
        '
        'pnl_rack
        '
        Me.pnl_rack.AutoScroll = True
        Me.pnl_rack.BackColor = System.Drawing.SystemColors.ControlDark
        Me.pnl_rack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl_rack.Location = New System.Drawing.Point(10, 330)
        Me.pnl_rack.Name = "pnl_rack"
        Me.pnl_rack.Size = New System.Drawing.Size(350, 80)
        Me.pnl_rack.TabIndex = 0
        '
        'btn_end
        '
        Me.btn_end.Location = New System.Drawing.Point(260, 300)
        Me.btn_end.Name = "btn_end"
        Me.btn_end.Size = New System.Drawing.Size(100, 23)
        Me.btn_end.TabIndex = 1
        Me.btn_end.Text = "End Turn"
        Me.btn_end.UseVisualStyleBackColor = True
        '
        'btn_trade
        '
        Me.btn_trade.Location = New System.Drawing.Point(10, 300)
        Me.btn_trade.Name = "btn_trade"
        Me.btn_trade.Size = New System.Drawing.Size(100, 23)
        Me.btn_trade.TabIndex = 2
        Me.btn_trade.Text = "Trade Tile"
        Me.btn_trade.UseVisualStyleBackColor = True
        '
        'btn_i_am
        '
        Me.btn_i_am.Location = New System.Drawing.Point(135, 415)
        Me.btn_i_am.Name = "btn_i_am"
        Me.btn_i_am.Size = New System.Drawing.Size(100, 23)
        Me.btn_i_am.TabIndex = 3
        Me.btn_i_am.UseVisualStyleBackColor = True
        Me.btn_i_am.Visible = False
        '
        'pnl_actplayer
        '
        Me.pnl_actplayer.AutoScroll = True
        Me.pnl_actplayer.Location = New System.Drawing.Point(10, 50)
        Me.pnl_actplayer.Name = "pnl_actplayer"
        Me.pnl_actplayer.Size = New System.Drawing.Size(200, 30)
        Me.pnl_actplayer.TabIndex = 4
        '
        'pnl_players
        '
        Me.pnl_players.AutoScroll = True
        Me.pnl_players.Location = New System.Drawing.Point(10, 85)
        Me.pnl_players.Name = "pnl_players"
        Me.pnl_players.Size = New System.Drawing.Size(200, 125)
        Me.pnl_players.TabIndex = 5
        '
        'btn_select
        '
        Me.btn_select.Location = New System.Drawing.Point(185, 300)
        Me.btn_select.Name = "btn_select"
        Me.btn_select.Size = New System.Drawing.Size(100, 23)
        Me.btn_select.TabIndex = 6
        Me.btn_select.Text = "Select All"
        Me.btn_select.UseVisualStyleBackColor = True
        Me.btn_select.Visible = False
        '
        'btn_load_board
        '
        Me.btn_load_board.Location = New System.Drawing.Point(85, 10)
        Me.btn_load_board.Name = "btn_load_board"
        Me.btn_load_board.Size = New System.Drawing.Size(125, 23)
        Me.btn_load_board.TabIndex = 7
        Me.btn_load_board.Text = "Display Issue"
        Me.btn_load_board.UseVisualStyleBackColor = True
        '
        'lbl_stack
        '
        Me.lbl_stack.AutoSize = True
        Me.lbl_stack.Location = New System.Drawing.Point(10, 215)
        Me.lbl_stack.Name = "lbl_stack"
        Me.lbl_stack.Size = New System.Drawing.Size(72, 13)
        Me.lbl_stack.TabIndex = 8
        Me.lbl_stack.Text = "Tile in Stack :"
        '
        'btn_menu
        '
        Me.btn_menu.Location = New System.Drawing.Point(10, 10)
        Me.btn_menu.Name = "btn_menu"
        Me.btn_menu.Size = New System.Drawing.Size(60, 23)
        Me.btn_menu.TabIndex = 9
        Me.btn_menu.Text = "Menu"
        Me.btn_menu.UseVisualStyleBackColor = True
        '
        'btn_cancelplacement
        '
        Me.btn_cancelplacement.Location = New System.Drawing.Point(10, 300)
        Me.btn_cancelplacement.Name = "btn_cancelplacement"
        Me.btn_cancelplacement.Size = New System.Drawing.Size(150, 23)
        Me.btn_cancelplacement.TabIndex = 10
        Me.btn_cancelplacement.Text = "Cancel Placement"
        Me.btn_cancelplacement.UseVisualStyleBackColor = True
        Me.btn_cancelplacement.Visible = False
        '
        'Game
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(799, 469)
        Me.ControlBox = False
        Me.Controls.Add(Me.btn_cancelplacement)
        Me.Controls.Add(Me.btn_menu)
        Me.Controls.Add(Me.lbl_stack)
        Me.Controls.Add(Me.btn_load_board)
        Me.Controls.Add(Me.btn_select)
        Me.Controls.Add(Me.pnl_players)
        Me.Controls.Add(Me.pnl_actplayer)
        Me.Controls.Add(Me.btn_i_am)
        Me.Controls.Add(Me.btn_trade)
        Me.Controls.Add(Me.btn_end)
        Me.Controls.Add(Me.pnl_rack)
        Me.Controls.Add(Me.pnl_board)
        Me.MinimumSize = New System.Drawing.Size(485, 485)
        Me.Name = "Game"
        Me.Text = "Qwirkle-Game"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pnl_board As Panel
    Friend WithEvents pnl_rack As Panel
    Friend WithEvents btn_end As Button
    Friend WithEvents btn_trade As Button
    Friend WithEvents btn_i_am As Button
    Friend WithEvents pnl_actplayer As Panel
    Friend WithEvents pnl_players As Panel
    Friend WithEvents btn_select As Button
    Friend WithEvents btn_load_board As Button
    Friend WithEvents lbl_stack As Label
    Friend WithEvents btn_menu As Button
    Friend WithEvents btn_cancelplacement As Button
End Class
