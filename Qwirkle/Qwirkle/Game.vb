﻿Imports ClassQwirkle
Imports Qwirkle.Exceptions
Public Class Game
    Dim Players As List(Of Player) = Context.GetPlayers()
    Dim B As Board
    Dim Trade As Boolean = False
    Dim Place As Boolean = False
    Dim Stack_Empty As Boolean = False
    Dim Board As List(Of List(Of Tile)) = New List(Of List(Of Tile))
    Private Sub Menu_Context(sender As Object, e As KeyEventArgs) Handles Me.KeyDown, btn_end.KeyDown, btn_i_am.KeyDown, btn_trade.KeyDown, pnl_actplayer.KeyDown, pnl_board.KeyDown, pnl_players.KeyDown, pnl_rack.KeyDown, btn_select.KeyDown, btn_load_board.KeyDown, btn_menu.KeyDown, btn_cancelplacement.KeyDown
        'Procédure permettant l'affichage du menu contextuel
        'en cas d'appui sur la touche 'Echap' (Note cela désactive
        'également le fournisseur tant que ce menu contextuel
        'est affiché.
        If e.KeyCode.Equals(Keys.Escape) Then
            Me.Enabled = False
            Qwirkle.Menu_Context.Show()
        End If
    End Sub
    Private Sub btn_menu_Click() Handles btn_menu.Click
        'Seconde Procédure permettant l'affichage du menu
        'contextuel en cas d'appui sur le bouton Menu.
        Me.Enabled = False
        Qwirkle.Menu_Context.Show()
    End Sub
    Public Sub Game_Load() Handles MyBase.Load
        'Procédure de chargement du formulaire où l'on
        'créé le board et l'on initialise l'interface.
        'On affiche également le formulaire Load pendant
        'ce chargement. Il suspend aussi le tracé de
        'interface le temps que l'on ajoute tout les
        'éléments à tracer.
        Me.Enabled = False
        SuspendLayout()
        Qwirkle.Load.Show()
        Try
            B = New Board(Players)
        Catch Ex As Exception
            MsgBox(Ex.Message, MsgBoxStyle.Critical, Ex.GetType().ToString())
        End Try
        Settings.Show()
        Settings.Hide()
        Place = False
        Trade = False
        Stack_Empty = False
        pnl_board.Controls.Clear()
        pnl_board.Width = B.getBoardSizeY() + 10
        pnl_board.Height = B.getBoardSizeX() + 10
        Call Init()
        Call Played_Load()
        ResumeLayout()
        Me.Enabled = True
    End Sub
    Private Sub Played_Load()
        'Procédure permetant l'affichage ou la MàJ
        'du nom des joueurs ainsi que leur score.
        'Cela réaffiche également le rack du joueur.
        Me.pnl_rack.Visible = False
        btn_trade.Visible = False
        btn_end.Visible = False
        btn_cancelplacement.Visible = False
        Call UploadRack()
        Me.btn_i_am.Text = String.Format("I am {0}", B.getPlayers()(0).getName())
        Me.btn_i_am.Visible = True
        Me.btn_i_am.AutoSize = True
        Call UploadName()
        btn_i_am.Enabled = True
        MsgBox(String.Format("Turn of {0}", B.getPlayers()(0).getName()))
    End Sub
    Private Sub WaitingForPlayer() Handles btn_i_am.Click
        'Procédure "d'attente" de la confirmation
        'du prochain joueur afin d'afficher son rack
        If btn_i_am.Enabled = True Then
            Stack_Empty = B.Is_Stack_Empty
            If Stack_Empty = True Then
                btn_end.Visible = True
            Else
                btn_end.Visible = False
            End If
            btn_trade.Visible = True
            Me.pnl_rack.Visible = True
            Me.btn_i_am.Visible = False
            btn_i_am.Enabled = False
        End If
    End Sub
    Private Sub UploadName()
        'Cette procédure "détruit" les noms
        'avant de régénérer les noms et les scores.
        Me.pnl_actplayer.Controls.Clear()
        Dim lblact As Label = New Label()
        lblact.Name = "lblact"
        lblact.Text = String.Format("{0} : {1} points", B.getPlayers()(0).getName(), B.getPlayers()(0).getScore())
        lblact.ForeColor = Color.FromArgb(Integer.Parse(B.getPlayers()(0).getColor()))
        If lblact.ForeColor = Color.White Then
            lblact.BackColor = Color.Black
        End If
        lblact.Size = New Size(75, 13)
        lblact.AutoSize = True
        lblact.Location = New Point(0, 0)
        Me.pnl_actplayer.Controls.Add(lblact)
        If Place = True Then
            Dim Test As Boolean = True
            Dim result As Short
            Try
                result = B.Calcul_Score()
            Catch ex As NoTilePlacedInThisTurnException
                Test = False
            End Try
            If Test = True Then
                Dim score As Label = New Label()
                score.Name = "lblscore"
                score.Text = String.Format("+ {0} points", result)
                score.ForeColor = Color.FromArgb(Integer.Parse(B.getPlayers()(0).getColor()))
                If score.ForeColor = Color.White Then
                    score.BackColor = Color.Black
                End If
                score.Size = New Size(75, 13)
                score.AutoSize = True
                score.Location = New Point(10, 15)
                Me.pnl_actplayer.Controls.Add(score)
            End If
        End If
        Me.pnl_players.Controls.Clear()
        For i = 1 To B.getPlayers().Count - 1
            Dim lbl As Label = New Label()
            lbl.Name = "lblact"
            lbl.Text = String.Format("{0} : {1} points", B.getPlayers()(i).getName(), B.getPlayers()(i).getScore())
            lbl.Size = New Size(125, 13)
            lbl.AutoSize = True
            lbl.Location = New Point(0, 13 * i)
            lbl.ForeColor = Color.FromArgb(Integer.Parse(B.getPlayers()(i).getColor()))
            If lbl.ForeColor = Color.White Then
                lbl.BackColor = Color.Black
            End If
            If Qwirkle.Menu_Context.GetGive_Up().Count > 0 Then
                For Each player As Player In Qwirkle.Menu_Context.GetGive_Up()
                    If player = B.getPlayers()(i) Then
                        lbl.Text = String.Format("{0} (Give Up)", lbl.Text)
                    End If
                Next
            End If
            Me.pnl_players.Controls.Add(lbl)
            AddHandler Me.pnl_players.Controls.Item(String.Format("lblact")).KeyDown, AddressOf Menu_Context
        Next
        lbl_stack.Text = String.Format("Tile in Stack : {0}", B.getStack().Count)
    End Sub
    Public Sub Reset()
        'Procédure de Reset, remettant à 0
        'l'affichage du rack et du Board
        Call ResetRack()
        Call ResetBoard()
    End Sub
    Private Sub ResetBoard()
        'Cela supprime l'affichage du Board
        'et la copie du board au sein du formulaire.
        Me.pnl_board.Controls.Clear()
        Board.Clear()
    End Sub
    Private Sub ResetRack()
        'Cela supprime l'affichage du Rack
        Me.pnl_rack.Controls.Clear()
    End Sub
    Private Sub Init()
        'Procédure d'Init, affichant ou
        'mettant à jour les Board et le
        'Rack.
        Call UploadBoard()
        Call UploadRack()
    End Sub
    Private Sub MàJ()
        'Procédure de MàJ, mettant à jour
        'les Board et le Rack tout en
        'suspendant le dessin de l'interface
        'celui-ci n'est repris qu'à la fin.
        Me.Enabled = False
        Qwirkle.Load.Show()
        SuspendLayout()
        Call ResetRack()
        Call Init()
        ResumeLayout()
        Me.Enabled = True
    End Sub
    Private Sub CreateTile(i As Integer, j As Integer)
        'Créé la picture box i, j.
        Dim color As Short = B.getBoard()(i)(j).getColor()
        Dim form As Short = B.getBoard()(i)(j).getShape()
        Dim ptcbox As PictureBox = New PictureBox()
        ptcbox.Name = String.Format("ptcbox_{0}_{1}", i, j)
        ptcbox.Size = New Size(B.getTileSizeY(), B.getTileSizeX())
        ptcbox.SizeMode = PictureBoxSizeMode.StretchImage
        ptcbox.Location = New Point((20 + (i * B.getTileSizeY())), (20 + (j * B.getTileSizeX())))
        Me.pnl_board.Controls.Add(ptcbox)
        AddHandler Me.pnl_board.Controls.Item(String.Format("ptcbox_{0}_{1}", i, j)).KeyDown, AddressOf Menu_Context
        If color = -1 And form = -1 Then
            ptcbox.Image = My.Resources.ResourceManager.GetObject("_1_1")
            AddHandler Me.pnl_board.Controls.Item(String.Format("ptcbox_{0}_{1}", i, j)).DragEnter, AddressOf BoardDragEnter
            AddHandler Me.pnl_board.Controls.Item(String.Format("ptcbox_{0}_{1}", i, j)).DragDrop, AddressOf BoardDragDrop
            Me.pnl_board.Controls.Item(String.Format("ptcbox_{0}_{1}", i, j)).AllowDrop = True
        Else
            ptcbox.Image = My.Resources.ResourceManager.GetObject(String.Format("_{0}{1}", form, color))
            Me.pnl_board.Controls.Item(String.Format("ptcbox_{0}_{1}", i, j)).AllowDrop = False
        End If
    End Sub
    Private Sub UploadBoard()
        'Mise à jour du Board, cela parcourt
        'le plateau et si la tuile n'existe pas
        'encore elle est alors dessinée dans le cas
        'contraire on vérifie que la version actuelle
        'correspond à la version affichée en ce cas on
        'ne change rien, sinon la tuile est détruite
        'avant que ne soit créer la nouvelle. Si la
        'tuile est nulle alors on associe les procédures
        'd'allowdrop et on l'autorise.
        For i = 0 To B.getNbLine() - 1
            For j = 0 To B.getNbColumn() - 1
                If Me.pnl_board.Controls.ContainsKey(String.Format("ptcbox_{0}_{1}", i, j)) = False Then
                    Call CreateTile(i, j)
                Else
                    Dim equal As Boolean = False
                    If (Board.Count > i) And (Board(i).Count > j) Then
                        If Board(i)(j) = B.getBoard()(i)(j) Then
                            equal = True
                        End If
                    End If
                    If equal = False Then
                        Me.pnl_board.Controls.RemoveByKey(String.Format("ptcbox_{0}_{1}", i, j))
                        Call CreateTile(i, j)
                    End If
                End If
            Next
        Next
        Board.Clear()
        For i = 0 To B.getNbLine() - 1
            Dim Line As List(Of Tile) = New List(Of Tile)
            Board.Add(Line)
            For j = 0 To B.getNbColumn() - 1
                Board(i).Add(B.getBoard()(i)(j))
            Next
        Next
    End Sub
    Private Sub UploadRack()
        'Création du rack du joueur.
        btn_trade.Size = New Size(100, 23)
        For i = 0 To B.getPlayers()(0).getRack().Count - 1
            Dim color As Short = B.getPlayers()(0).getRack()(i).getColor()
            Dim form As Short = B.getPlayers()(0).getRack()(i).getShape()
            Dim ptcbox As PictureBox = New PictureBox()
            ptcbox.Name = String.Format("ptcboxj{0}", i)
            ptcbox.Size = New Size(50, 50)
            ptcbox.Image = My.Resources.ResourceManager.GetObject(String.Format("_{0}{1}", form, color))
            ptcbox.SizeMode = PictureBoxSizeMode.StretchImage
            ptcbox.AllowDrop = False
            ptcbox.Location = New Point((10 + (i * 55)), 10)
            Me.pnl_rack.Controls.Add(ptcbox)
            AddHandler Me.pnl_rack.Controls.Item(String.Format("ptcboxj{0}", i)).MouseMove, AddressOf SaisiePictRack
            AddHandler Me.pnl_rack.Controls.Item(String.Format("ptcboxj{0}", i)).KeyDown, AddressOf Menu_Context
        Next
    End Sub
    Private Sub TradeRack()
        'Création du rack avec les checkbox d'échange.
        btn_trade.Size = New Size(100, 23)
        For i = 0 To B.getPlayers()(0).getRack().Count - 1
            Dim color As Short = B.getPlayers()(0).getRack()(i).getColor()
            Dim form As Short = B.getPlayers()(0).getRack()(i).getShape()
            Dim ptcbox As PictureBox = New PictureBox()
            Dim check As CheckBox = New CheckBox()
            ptcbox.Name = String.Format("ptcboxj{0}", i)
            ptcbox.Size = New Size(50, 50)
            ptcbox.Image = My.Resources.ResourceManager.GetObject(String.Format("_{0}{1}", form, color))
            ptcbox.SizeMode = PictureBoxSizeMode.StretchImage
            ptcbox.AllowDrop = False
            ptcbox.Location = New Point((i * 65), 10)
            check.Name = String.Format("chcbox{0}", i)
            check.Checked = False
            check.Location = New Point((50 + (i * 65)), 10)
            check.Size = New Size(15, 15)
            Me.pnl_rack.Controls.Add(check)
            Me.pnl_rack.Controls.Add(ptcbox)
            AddHandler Me.pnl_rack.Controls.Item(String.Format("chcbox{0}", i)).Click, AddressOf CheckedCheckboxTrade
            AddHandler Me.pnl_rack.Controls.Item(String.Format("chcbox{0}", i)).KeyDown, AddressOf Menu_Context
            AddHandler Me.pnl_rack.Controls.Item(String.Format("ptcboxj{0}", i)).KeyDown, AddressOf Menu_Context
        Next
    End Sub
    Private Sub CheckedCheckboxTrade()
        'Procédure permetant de mettre à jour les boutons
        'd'Echange.
        Dim compt As Short = 0
        For i = 0 To B.getPlayers()(0).getRack().Count - 1
            Dim check As CheckBox = Me.pnl_rack.Controls.Item(String.Format("chcbox{0}", i))
            If check.Checked = True Then
                compt = compt + 1
            End If
        Next
        If compt > 0 Then
            btn_trade.Text = "Trade ! (And End Turn)"
            btn_trade.Size = New Size(150, 23)
            If compt = B.getPlayers()(0).getRack().Count Then
                btn_select.Text = "UnSelect All"
            End If
        Else
            btn_trade.Text = "Abort ?"
            btn_trade.Size = New Size(100, 23)
        End If
    End Sub
    Private Sub SaisiePictRack(sender As Object, e As MouseEventArgs)
        'Procédure de saisie d'une tuile du rack.
        Dim pic As PictureBox = sender
        Dim rep As DragDropEffects
        If e.Button = MouseButtons.Left Then
            rep = pic.DoDragDrop(pic, DragDropEffects.Move)
            If rep = DragDropEffects.Move Then
                rep = Nothing
            End If
        End If
    End Sub
    Private Sub BoardDragEnter(sender As Object, e As DragEventArgs)
        'Procédure de DragEnter sur le Board.
        If e.Data.GetDataPresent("System.Windows.Forms.PictureBox") Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub
    Private Sub BoardDragDrop(sender As Object, e As DragEventArgs)
        'Procédure de DragDrop où l'on teste de poser
        'la tuile, si cela n'est pas possible alors une
        'Exception est levé et renvoyée dans une messageBox.
        'Si c'est la première tuile à être posé on masque
        'l'option d'échange désormais plus disponible alors
        'qu'on affiche la fin de tour si cela n'était pas
        'encore disponible.
        Dim pic As PictureBox = sender
        Dim x As Short = Short.Parse(pic.Name.Split("_")(1))
        Dim y As Short = Short.Parse(pic.Name.Split("_")(2))
        Dim pic2 As PictureBox = e.Data.GetData("System.Windows.Forms.PictureBox")
        Dim index As Short = Short.Parse(pic2.Name.Chars(pic2.Name.Count - 1))
        Dim test As Boolean = True
        Try
            B.getPlayers()(0).Place_Tiles(B, B.getPlayers()(0).getRack()(index), x, y)
        Catch Ex As Exception
            If Ex.GetType().IsInstanceOfType(New WrongPositionForTileException) Then
                Dim Exc As WrongPositionForTileException = Ex
                MsgBox(String.Format("{0} Reason : {1}", Exc.Message, Exc.GetComplement()), MsgBoxStyle.Critical, "Wrong Position For Tile")
            Else
                MsgBox(Ex.Message, MsgBoxStyle.Critical, Ex.GetType().Name)
            End If
            test = False
        End Try
        If test = True Then
            'Si le plateau est à mettre à jour, l'affichage
            'est suspendu, l'interface est retracée et si le
            'plateau dépasse du panel afin de palier à tout
            'soucis d'affichage, on replace les 2 scrollbars
            'tout en haut du panel tout en sauvegardant au
            'préalable les valeurs de celles-ci avant de
            'retracer l'interface et puis de replacer les
            'scrollbars dans leur état initial.
            Me.Enabled = False
            Qwirkle.Load.Show()
            SuspendLayout()
            pnl_board.HorizontalScroll.Value = 0
            pnl_board.VerticalScroll.Value = 0
            If Place = False Then
                Place = True
                btn_trade.Visible = False
                btn_end.Visible = True
            End If
            If btn_cancelplacement.Visible = False Then
                btn_cancelplacement.Visible = True
            End If
            Call UploadName()
            Call MàJ()
            ResumeLayout()
            Me.Enabled = True
        End If
    End Sub
    Public Sub NextTurn()
        'Méthode de passage de tour et vérification
        'que la partie ne se termine pas prématurément.
        Place = False
        Trade = False
        Dim alone As Boolean = False
        Dim skip As Boolean = False
        Dim Endgame As Boolean = False
        If Qwirkle.Menu_Context.GetGive_Up().Count > 0 Then
            If (B.getPlayers().Count - Qwirkle.Menu_Context.GetGive_Up().Count) = 1 Then
                alone = True
            Else
                For Each player As Player In Qwirkle.Menu_Context.GetGive_Up()
                    If player = B.getPlayers()(0) Then
                        skip = True
                    End If
                Next
                If skip = True Then
                    EndTurn()
                End If
                Dim tiles As List(Of Tile) = New List(Of Tile)()
                Dim ActPlayers As List(Of Player) = New List(Of Player)()
                For Each player As Player In B.getPlayers()
                    ActPlayers.Add(player)
                Next
                For Each player As Player In Qwirkle.Menu_Context.GetGive_Up()
                    ActPlayers.Remove(player)
                Next
                For l = 0 To ActPlayers.Count - 1
                    For k = 0 To ActPlayers(l).getRack().Count - 1
                        tiles.Add(ActPlayers(l).getRack()(k))
                    Next
                Next
                If Stack_Empty = False Then
                    tiles.AddRange(B.getStack())
                End If
                If B.PossibilityOfPlaying(tiles) = True Then
                    Endgame = False
                Else
                    Endgame = True
                End If
            End If
        End If
        If skip = False And alone = False And Endgame = False Then
            Call ResetRack()
            Call Played_Load()
            Me.Enabled = True
        End If
        If Endgame = True Or alone = True Then
            If Endgame = True Then
                B.getPlayers()(B.getPlayers().Count - 2).Increase_score(6)
            ElseIf alone = True Then
                B.getPlayers()(0).Increase_score(6)
            End If
            Finish.Finish_Load()
            Finish.Show()
            Me.Enabled = False
            Me.Hide()
        End If
    End Sub
    Public Sub EndTurn() Handles btn_end.Click
        'Procédure de fin de tour en prenant compte
        'des éventuels abandons et des valeurs de
        'retour de la méthode EndTurn
        Me.Enabled = False
        Qwirkle.Load.Show()
        Dim Fin As Integer = B.EndTurn()
        If Fin = 0 Then
            Call NextTurn()
        ElseIf Fin = 1 Then
            If Stack_Empty = False Then
                Stack_Empty = True
            End If
            Call NextTurn()
        ElseIf Fin = 2 Then
            Me.Enabled = True
            Me.Enabled = False
            Finish.Finish_Load()
            Finish.Show()
            Me.Hide()
        End If
    End Sub
    Private Sub btn_trade_Click() Handles btn_trade.Click
        'Procédure d'affichage de l'échange ou
        'd'échange si au moins une tuile est
        'échangée.
        If Trade = False Then
            btn_select.Text = "Select All"
            btn_trade.Text = "Abort ?"
            btn_trade.Size = New Size(100, 23)
            Trade = True
            btn_end.Visible = False
            btn_select.Visible = True
            Call ResetRack()
            Call TradeRack()
        Else
            Dim compt As Short = 0
            For i = 0 To B.getPlayers()(0).getRack().Count - 1
                Dim check As CheckBox = Me.pnl_rack.Controls.Item(String.Format("chcbox{0}", i))
                If check.Checked = True Then
                    compt = compt + 1
                End If
            Next
            If compt > 0 Then
                Dim Exchange As List(Of Tile) = New List(Of Tile)()
                For i = 0 To B.getPlayers()(0).getRack().Count - 1
                    Dim check As CheckBox = Me.pnl_rack.Controls.Item(String.Format("chcbox{0}", i))
                    If check.Checked = True Then
                        Dim Index As Short = Short.Parse(check.Name.Chars(check.Name.Count - 1))
                        Dim Tile As Tile = B.getPlayers()(0).getRack()(Index)
                        Exchange.Add(Tile)
                    End If
                Next
                Try
                    B.getPlayers()(0).Trade_Tiles(B, Exchange)
                Catch Ex As StackIsEmptyException
                    If Stack_Empty = False Then
                        Stack_Empty = True
                    End If
                End Try
            End If
            btn_trade.Text = "Trade Tile"
            btn_trade.Size = New Size(100, 23)
            btn_select.Visible = False
            Trade = False
            If Stack_Empty = True And Place = True Then
                btn_end.Visible = True
            End If
            If compt > 0 Then
                Call ResetRack()
                Call EndTurn()
            Else
                Call ResetRack()
                Call UploadRack()
            End If
        End If
    End Sub
    Public Function GetBoard() As Board
        'Accesseur du Board'
        Return B
    End Function
    Private Sub btn_select_Click() Handles btn_select.Click
        'Fonction de sélection de toutes les tuiles
        'disponibles à l'échange ou à contrario de
        'tout déselectionner.
        If Trade = True Then
            If btn_select.Text = "Select All" Then
                For i = 0 To B.getPlayers()(0).getRack().Count - 1
                    Dim check As CheckBox = Me.pnl_rack.Controls.Item(String.Format("chcbox{0}", i))
                    check.Checked = True
                Next
                btn_select.Text = "UnSelect All"
                btn_trade.Text = "Trade ! (And End Turn)"
                btn_trade.Size = New Size(150, 23)
            ElseIf btn_select.Text = "UnSelect All" Then
                For i = 0 To B.getPlayers()(0).getRack().Count - 1
                    Dim check As CheckBox = Me.pnl_rack.Controls.Item(String.Format("chcbox{0}", i))
                    check.Checked = False
                Next
                btn_select.Text = "Select All"
                btn_trade.Text = "Abort ?"
                btn_trade.Size = New Size(100, 23)
            End If
        End If
    End Sub
    Private Sub btn_load_board_Click() Handles btn_load_board.Click
        'Methode pour recharger d'un coup l'interface en cas de
        'problèmes graphiques.
        Me.Enabled = False
        Qwirkle.Load.Show()
        SuspendLayout()
        Qwirkle.Load.Show()
        Call ResetBoard()
        Call UploadBoard()
        Call UploadRack()
        If btn_load_board.Text = "Display Issue" Then
            btn_load_board.Text = "Display Issue, Again ?"
        End If
        ResumeLayout()
        Me.Enabled = True
    End Sub
    Public Sub ReloadBoard()
        'Methode pour mettre à jour le board suite à des modification
        'graphiques.
        If B.getBoardSizeX() <> Settings.GetBoardSizeX() Or B.getBoardSizeY() <> Settings.GetBoardSizeY() Or B.getNbColumnDisplay() <> Settings.GetNbColumnDisplay() Or B.getNbLineDisplay() <> Settings.GetNbLineDisplay() Then
            Dim Test As Boolean = False
            If B.getBoardSizeX() <> Settings.GetBoardSizeX() Then
                B.setBoardSizeX(Settings.GetBoardSizeX())
                Test = True
            End If
            If B.getBoardSizeY() <> Settings.GetBoardSizeY() Then
                B.setBoardSizeY(Settings.GetBoardSizeY())
                Test = True
            End If
            If B.getNbColumn() <> Settings.GetNbColumnDisplay() Then
                B.setNbColumnDisplay(Settings.GetNbColumnDisplay())
                Test = True
            End If
            If B.getNbLine() <> Settings.GetNbLineDisplay() Then
                B.setNbLineDisplay(Settings.GetNbLineDisplay())
                Test = True
            End If
            If Test = True Then
                SuspendLayout()
                Board.Clear()
                Me.Enabled = False
                Qwirkle.Load.Show()
                pnl_board.Controls.Clear()
                pnl_board.Width = B.getBoardSizeY() + 10
                pnl_board.Height = B.getBoardSizeX() + 10
                Call UploadBoard()
                ResumeLayout()
                Me.Enabled = True
            End If
        End If
    End Sub
    Private Sub btn_cancelplacement_Click() Handles btn_cancelplacement.Click
        'Méthode appelant la méthode du board CancelPlacement et recharge
        'le board et le rack en plus de la prévisualisation du score.
        Dim Test As Boolean = True
        Try
            B.CancelPlacement()
        Catch ex As NoTilePlacedInThisTurnException
            MsgBox(ex.Message)
            Test = False
        End Try
        If Test = True Then
            Me.Enabled = False
            Qwirkle.Load.Show()
            SuspendLayout()
            Call ResetBoard()
            Call UploadBoard()
            Call UploadRack()
            Call UploadName()
            btn_cancelplacement.Visible = False
            ResumeLayout()
            Me.Enabled = True
        End If
    End Sub
End Class