﻿Imports ClassQwirkle
Imports Qwirkle.Exceptions
Public Class Context
    Dim Players As New List(Of Player)
    Dim NbPlayers As Short = 2
    Dim ListControls As New List(Of Control)
    Dim ListColor As New List(Of Color)
    Dim ListName As New List(Of String)
    Dim ListAge As New List(Of Short)
    Private Sub Init()
    'Procédure de création des champs de saisie
    'et des color-picker pour le nombre de joueurs
    'spécifié dans NbPlayers.
    For i = 1 To NbPlayers
            Dim lbl As New Label
            Dim txt As New TextBox
            Dim lbcolor As New Label
            Dim numupdown As New NumericUpDown
            lbl.Name = String.Format("lbl{0}", i)
            lbl.Text = String.Format("Player {0}", i)
            lbl.Size = New Size(50, 13)
            txt.Name = String.Format("txt{0}", i)
            txt.Size = New Size(100, 20)
            If ListName.Count >= i Then
                txt.Text = ListName(i - 1)
            End If
            lbcolor.Name = String.Format("lbcolor{0}", i)
            lbcolor.Size = New Size(12, 15)
            lbcolor.BorderStyle = BorderStyle.FixedSingle
            If ListColor.Count >= i Then
                lbcolor.BackColor = ListColor(i - 1)
            Else
                lbcolor.BackColor = Color.Black
            End If
            numupdown.Name = String.Format("numupdown{0}", i)
            numupdown.Value = 1
            numupdown.Size = New Size(50, 13)
            numupdown.Maximum = 125
            If ListAge.Count >= i Then
                numupdown.Value = ListAge(i - 1)
            End If
            If i = 1 Then
                Dim lbltitle As New Label
                lbltitle.Name = "lbltitle"
                lbltitle.Text = "Choose a name and pick a color"
                lbltitle.Size = New Size(211, 17)
                lbltitle.Location = New Point(320, 220 - 20 * (NbPlayers + 2))
                lbl.Location = New Point(290, 220 - 20 * NbPlayers)
                txt.Location = New Point(390, 220 - 20 * NbPlayers)
                lbcolor.Location = New Point(500, 220 - 20 * NbPlayers)
                numupdown.Location = New Point(525, 220 - 20 * NbPlayers)
                Me.Controls.Add(lbltitle)
                ListControls.Add(lbltitle)
            Else
                Dim lblTemp As New Label
                Dim txtTemp As New TextBox
                Dim lbcolorTemp As New Label
                Dim numupdownTemp As New NumericUpDown
                lblTemp = Me.Controls(String.Format("lbl{0}", i - 1))
                txtTemp = Me.Controls(String.Format("txt{0}", i - 1))
                lbcolorTemp = Me.Controls(String.Format("lbcolor{0}", i - 1))
                numupdownTemp = Me.Controls(String.Format("numupdown{0}", i - 1))
                lbl.Location = New Point(290, lblTemp.Location.Y + 40)
                txt.Location = New Point(390, txtTemp.Location.Y + 40)
                lbcolor.Location = New Point(500, lbcolorTemp.Location.Y + 40)
                numupdown.Location = New Point(525, lbcolorTemp.Location.Y + 40)
            End If
            Me.Controls.Add(lbl)
            Me.Controls.Add(txt)
            Me.Controls.Add(lbcolor)
            Me.Controls.Add(numupdown)
            ListControls.Add(lbl)
            ListControls.Add(txt)
            ListControls.Add(lbcolor)
            ListControls.Add(numupdown)
            AddHandler Me.Controls.Item(String.Format("lbcolor{0}", i)).Click, AddressOf lbcolormethod
        Next
        If NbPlayers = 2 Then
            btnRmPlayer.Visible = False
        ElseIf NbPlayers = 3 Then
            btnAddPlayer.Visible = True
            btnRmPlayer.Visible = True
        ElseIf NbPlayers = 4 Then
            btnAddPlayer.Visible = False
        End If
    End Sub
    Private Sub ResetGraphique()
        'Destruction des champs de saisie et des color-picker
        'préalablement crée (car présents dans la liste
        'ListControls)
        For Each Ctrl As Control In ListControls
            Me.Controls.Remove(Ctrl)
        Next
        ListControls.Clear()
    End Sub
    Private Sub Reset()
        'Reset complet du formulaire
        Call ResetGraphique()
        ListAge.Clear()
        ListColor.Clear()
        ListName.Clear()
    End Sub
    Private Sub Lbcolormethod(ByVal sender As Object, ByVal e As EventArgs)
        'Procédure d'affichage du color-picker.
        'Une fois affiché, si on confirme une couleur
        'cela modifie la couleur de fond du label.
        Dim cDialog As New ColorDialog()
        cDialog.Color = sender.BackColor
        If (cDialog.ShowDialog() = DialogResult.OK) Then
            sender.BackColor = cDialog.Color
        End If
    End Sub
    Private Sub Context_Load() Handles MyBase.Load
        'Appel des procédures de reset et de création
        'des champs lors du chargement du formulaire.
        Players.Clear()
        Call Init()
    End Sub
    Private Sub BtnReturnToMenu_Click() Handles btnReturnToMenu.Click
        'Retour vers le Menu.
        Me.Close()
        Qwirkle.Menu.Show()
    End Sub
    Private Sub Save_Value()
        'Sauvegarde les valeurs des champs
        'avant un ajout ou une suppression
        'de champ.
        ListAge.Clear()
        ListColor.Clear()
        ListName.Clear()
        For i = 1 To NbPlayers
            ListColor.Add(Me.Controls.Item(String.Format("lbcolor{0}", i)).BackColor)
            ListName.Add(Me.Controls.Item(String.Format("txt{0}", i)).Text)
            ListAge.Add(Short.Parse(Me.Controls.Item(String.Format("numupdown{0}", i)).Text))
        Next
    End Sub
    Private Sub Btnaddplayer_Click() Handles btnAddPlayer.Click
        'Incrémente NbPlayer si il est inférieur à 4
        'avant de mettre à jour l'affichage des champs.
        If NbPlayers < 4 Then
            Call Save_Value()
            NbPlayers = NbPlayers + 1
            Call ResetGraphique()
            Call Init()
        End If
    End Sub
    Private Sub BtnRmPlayer_Click() Handles btnRmPlayer.Click
        'Décrémente NbPlayer si il est supérieur à 2
        'avant de mettre à jour l'affichage des champs.
        If NbPlayers > 2 Then
            Call Save_Value()
            NbPlayers = NbPlayers - 1
            Call ResetGraphique()
            Call Init()
        End If
    End Sub
    Private Sub BtnStart_Click() Handles btnStart.Click
        'Procédure de lancement de la partie, si les champs
        'ne sont pas tous complétés, la procédure renvoie sous
        'forme de message box que tout les joueurs doivent être
        'complétés.
        Dim Verif As Boolean = True
        For i = 1 To NbPlayers
            If Me.Controls.Item(String.Format("txt{0}", i)).Text = "" Then
                Verif = False
            End If
        Next
        If Verif = False Then
            MsgBox("All Players must be completed.")
        Else
            For i = 1 To NbPlayers
                Dim P As Player = New Player(Me.Controls.Item(String.Format("lbcolor{0}", i)).BackColor.ToArgb().ToString(), Me.Controls.Item(String.Format("txt{0}", i)).Text, Short.Parse(Me.Controls.Item(String.Format("numupdown{0}", i)).Text))
                Players.Add(P)
            Next
            Me.Hide()
            Menu_Context.Reset()
            Game.Show()
        End If
    End Sub
    Public Function GetPlayers() As List(Of Player)
        'Accesseur de la list de Joueurs
        Return Players
    End Function
    Public Sub SetPlayers(players As List(Of Player))
        'Redéfinition de la liste de Joueurs
        Me.Players.Clear()
        Me.Players.AddRange(players)
    End Sub
End Class