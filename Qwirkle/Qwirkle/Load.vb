﻿Public Class Load
    Dim Authorize As Boolean = False
    Private Async Function Form_Wait() As Task(Of Boolean)
        'Cette procédure attend que Game soit activé
        'avant de return true (On vérifie toutes les
        'secondes)
        Dim compt As Short = 0
        While Qwirkle.Game.Enabled = False And Qwirkle.Finish.Visible = False
            Await Task.Delay(10)
            If compt < 100 Then
                compt += 1
            Else
                Me.Cursor = Cursors.WaitCursor
                Qwirkle.Game.Cursor = Cursors.WaitCursor
            End If
        End While
        If Qwirkle.Game.Enabled = True Or Qwirkle.Finish.Visible = True Then
            Me.Cursor = Cursors.Default
            Qwirkle.Game.Cursor = Cursors.Default
            Return True
        End If
        Return False
    End Function
    Private Async Sub Form_Load() Handles MyBase.Load
        'Cette procédure lancé au chargement
        'du formulaire attend un true de la part
        'de la méthode Form_Wait avant de fermer
        'le formulaire et de rendre le focus
        'à la fenêtre Game.
        Authorize = False
        Authorize = Await Form_Wait()
        Qwirkle.Game.Show()
        Me.Close()
    End Sub
    Private Sub Form_Close() Handles Me.Closed
        'On vérifie que la fermeture est du
        'à la méthode Form_Wait sinon on indique
        'via message box à l'utilisateur de patienter.
        If Authorize = False Then
            Me.Show()
            MsgBox("Please Wait Game Loading...", MsgBoxStyle.ApplicationModal, "Please Wait")
        End If
    End Sub
End Class