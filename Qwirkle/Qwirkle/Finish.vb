﻿Imports ClassQwirkle
Public Class Finish
    Dim B As Board = Game.GetBoard()
    Public Sub Finish_Load() Handles MyBase.Load
        'Procédure de chargement du formulaire
        'où toute l'interface est créée et où les
        'joueurs sont classé en fonction de leur
        'score.
        B = Game.GetBoard()
        Me.Controls.Clear()
        Dim Players As List(Of Player) = New List(Of Player)()
        Players.Add(B.getPlayers()(0))
        For i = 1 To B.getPlayers().Count - 1
            Dim j As Integer = 0
            While j < Players.Count - 1 And B.getPlayers()(i).getScore() > Players(j).getScore()
                j = j + 1
            End While
            If B.getPlayers()(i).getScore() > Players(j).getScore() Then
                j = j + 1
            End If
            Players.Insert(j, B.getPlayers()(i))
        Next
        For i = 0 To Players.Count - 1
            Dim lblact As Label = New Label()
            lblact.Name = String.Format("lblact{0}", i)
            lblact.Text = String.Format("{0} : {1} points", Players(Players.Count - (i + 1)).getName(), Players(Players.Count - (i + 1)).getScore())
            If Qwirkle.Menu_Context.GetGive_Up().Count > 0 Then
                For Each player As Player In Qwirkle.Menu_Context.GetGive_Up()
                    If player = Players(Players.Count - (i + 1)) Then
                        lblact.Text = String.Format("{0} (Give Up)", lblact.Text)
                    End If
                Next
            End If
            lblact.ForeColor = Color.FromArgb(Integer.Parse(Players(Players.Count - (i + 1)).getColor()))
            If lblact.ForeColor = Color.White Then
                lblact.BackColor = Color.Black
            End If
            lblact.Size = New Size(125, 13)
            lblact.Location = New Point(10, 10 + 13 * i)
            Me.Controls.Add(lblact)
        Next
        Dim btnr As Button = New Button()
        btnr.Name = "btnrestart"
        btnr.Text = "ReStart"
        btnr.Location = New Point(10, 10 + 13 * (Players.Count))
        btnr.Size = New Size(75, 25)
        Me.Controls.Add(btnr)
        AddHandler Me.Controls.Item(String.Format("btnrestart")).Click, AddressOf Restart
        Dim btn2 As Button = New Button()
        btn2.Name = "btnquit"
        btn2.Text = "Exit"
        btn2.Location = New Point(10, 35 + 13 * (Players.Count))
        btn2.Size = New Size(75, 25)
        Me.Controls.Add(btn2)
        AddHandler Me.Controls.Item(String.Format("btnquit")).Click, AddressOf Quit
        Me.Size = New Size(150, 100 + 13 * (Players.Count))
    End Sub
    Private Sub Quit() Handles Me.Closed
        'Ferme toute l'application.
        Qwirkle.Menu.Close()
    End Sub
    Private Sub Restart()
        'Relance une nouvelle partie avec des
        'joueurs portant le même nom et les mêmes
        'couleurs. (Et donc le même nombre de joueurs)
        Dim players As List(Of Player) = New List(Of Player)()
        For i = 0 To Context.GetPlayers().Count - 1
            Dim player As Player = New Player(Context.GetPlayers()(i).getColor(), Context.GetPlayers()(i).getName(), Context.GetPlayers()(i).getAge())
            players.Add(player)
        Next
        Qwirkle.Context.SetPlayers(players)
        Qwirkle.Menu_Context.Reset()
        Qwirkle.Game.Reset()
        Qwirkle.Game.Game_Load()
        Qwirkle.Game.Enabled = True
        Qwirkle.Game.Show()
        Me.Hide()
    End Sub
End Class