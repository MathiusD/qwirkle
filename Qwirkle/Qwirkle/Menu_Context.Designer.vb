﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Menu_Context
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_title = New System.Windows.Forms.Label()
        Me.btn_return = New System.Windows.Forms.Button()
        Me.btn_exit = New System.Windows.Forms.Button()
        Me.btn_restart = New System.Windows.Forms.Button()
        Me.btn_giveup = New System.Windows.Forms.Button()
        Me.btn_settings = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lbl_title
        '
        Me.lbl_title.AutoSize = True
        Me.lbl_title.Location = New System.Drawing.Point(45, 15)
        Me.lbl_title.Name = "lbl_title"
        Me.lbl_title.Size = New System.Drawing.Size(85, 13)
        Me.lbl_title.TabIndex = 0
        Me.lbl_title.Text = "Game Is Paused"
        '
        'btn_return
        '
        Me.btn_return.Location = New System.Drawing.Point(50, 35)
        Me.btn_return.Name = "btn_return"
        Me.btn_return.Size = New System.Drawing.Size(75, 23)
        Me.btn_return.TabIndex = 1
        Me.btn_return.Text = "Return"
        Me.btn_return.UseVisualStyleBackColor = True
        '
        'btn_exit
        '
        Me.btn_exit.Location = New System.Drawing.Point(50, 135)
        Me.btn_exit.Name = "btn_exit"
        Me.btn_exit.Size = New System.Drawing.Size(75, 23)
        Me.btn_exit.TabIndex = 4
        Me.btn_exit.Text = "EXIT"
        Me.btn_exit.UseVisualStyleBackColor = True
        '
        'btn_restart
        '
        Me.btn_restart.Location = New System.Drawing.Point(50, 110)
        Me.btn_restart.Name = "btn_restart"
        Me.btn_restart.Size = New System.Drawing.Size(75, 23)
        Me.btn_restart.TabIndex = 3
        Me.btn_restart.Text = "Restart"
        Me.btn_restart.UseVisualStyleBackColor = True
        '
        'btn_giveup
        '
        Me.btn_giveup.Location = New System.Drawing.Point(50, 85)
        Me.btn_giveup.Name = "btn_giveup"
        Me.btn_giveup.Size = New System.Drawing.Size(75, 23)
        Me.btn_giveup.TabIndex = 2
        Me.btn_giveup.Text = "Give Up"
        Me.btn_giveup.UseVisualStyleBackColor = True
        '
        'btn_settings
        '
        Me.btn_settings.Location = New System.Drawing.Point(50, 60)
        Me.btn_settings.Name = "btn_settings"
        Me.btn_settings.Size = New System.Drawing.Size(75, 23)
        Me.btn_settings.TabIndex = 5
        Me.btn_settings.Text = "Settings"
        Me.btn_settings.UseVisualStyleBackColor = True
        '
        'Menu_Context
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(184, 162)
        Me.ControlBox = False
        Me.CenterToScreen()
        Me.Controls.Add(Me.btn_settings)
        Me.Controls.Add(Me.btn_giveup)
        Me.Controls.Add(Me.btn_restart)
        Me.Controls.Add(Me.btn_exit)
        Me.Controls.Add(Me.btn_return)
        Me.Controls.Add(Me.lbl_title)
        Me.Name = "Menu_Context"
        Me.Text = "Qwirkle-Menu"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_title As Label
    Friend WithEvents btn_return As Button
    Friend WithEvents btn_exit As Button
    Friend WithEvents btn_restart As Button
    Friend WithEvents btn_giveup As Button
    Friend WithEvents btn_settings As Button
End Class
