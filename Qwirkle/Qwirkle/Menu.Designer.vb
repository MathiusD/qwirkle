﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Menu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.btnInfos = New System.Windows.Forms.Button()
        Me.btnRules = New System.Windows.Forms.Button()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnStart
        '
        Me.btnStart.Location = New System.Drawing.Point(330, 200)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(140, 30)
        Me.btnStart.TabIndex = 1
        Me.btnStart.Text = "Start"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'btnInfos
        '
        Me.btnInfos.Location = New System.Drawing.Point(330, 320)
        Me.btnInfos.Name = "btnInfos"
        Me.btnInfos.Size = New System.Drawing.Size(140, 30)
        Me.btnInfos.TabIndex = 3
        Me.btnInfos.Text = "Infos"
        Me.btnInfos.UseVisualStyleBackColor = True
        '
        'btnRules
        '
        Me.btnRules.Location = New System.Drawing.Point(330, 260)
        Me.btnRules.Name = "btnRules"
        Me.btnRules.Size = New System.Drawing.Size(140, 30)
        Me.btnRules.TabIndex = 2
        Me.btnRules.Text = "Rules"
        Me.btnRules.UseVisualStyleBackColor = True
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!)
        Me.lblTitle.Location = New System.Drawing.Point(355, 132)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(92, 22)
        Me.lblTitle.TabIndex = 4
        Me.lblTitle.Text = "QWIRKLE"
        '
        'Menu
        '
        Me.AccessibleName = "frmMenu"
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 562)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.btnRules)
        Me.Controls.Add(Me.btnInfos)
        Me.Controls.Add(Me.btnStart)
        Me.Name = "Menu"
        Me.Text = "Qwirkle-Menu"
        Me.ResumeLayout(False)
        Me.PerformLayout()
        Me.CenterToScreen()
    End Sub

    Friend WithEvents btnStart As Button
    Friend WithEvents btnInfos As Button
    Friend WithEvents btnRules As Button
    Friend WithEvents lblTitle As Label
End Class
