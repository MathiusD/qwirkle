﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Context
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.btnReturnToMenu = New System.Windows.Forms.Button()
        Me.btnAddPlayer = New System.Windows.Forms.Button()
        Me.btnRmPlayer = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnStart
        '
        Me.btnStart.AccessibleName = "btnStart"
        Me.btnStart.Location = New System.Drawing.Point(338, 398)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(140, 30)
        Me.btnStart.TabIndex = 3
        Me.btnStart.Text = "Start"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'btnReturnToMenu
        '
        Me.btnReturnToMenu.AccessibleName = "btnQuit"
        Me.btnReturnToMenu.Location = New System.Drawing.Point(607, 493)
        Me.btnReturnToMenu.Name = "btnReturnToMenu"
        Me.btnReturnToMenu.Size = New System.Drawing.Size(140, 30)
        Me.btnReturnToMenu.TabIndex = 4
        Me.btnReturnToMenu.Text = "Return to menu"
        Me.btnReturnToMenu.UseVisualStyleBackColor = True
        '
        'btnAddPlayer
        '
        Me.btnAddPlayer.AccessibleName = "btnStart"
        Me.btnAddPlayer.Location = New System.Drawing.Point(435, 350)
        Me.btnAddPlayer.Name = "btnAddPlayer"
        Me.btnAddPlayer.Size = New System.Drawing.Size(70, 40)
        Me.btnAddPlayer.TabIndex = 2
        Me.btnAddPlayer.Text = "Add Player"
        Me.btnAddPlayer.UseVisualStyleBackColor = True
        '
        'btnRmPlayer
        '
        Me.btnRmPlayer.AccessibleName = "btnStart"
        Me.btnRmPlayer.Location = New System.Drawing.Point(295, 350)
        Me.btnRmPlayer.Name = "btnRmPlayer"
        Me.btnRmPlayer.Size = New System.Drawing.Size(70, 40)
        Me.btnRmPlayer.TabIndex = 1
        Me.btnRmPlayer.Text = "Remove Player"
        Me.btnRmPlayer.UseVisualStyleBackColor = True
        '
        'Context
        '
        Me.AccessibleName = "frmContext"
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 562)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnRmPlayer)
        Me.Controls.Add(Me.btnAddPlayer)
        Me.Controls.Add(Me.btnStart)
        Me.Controls.Add(Me.btnReturnToMenu)
        Me.Name = "Context"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Context"
        Me.ResumeLayout(False)
        Me.CenterToScreen()
    End Sub
    Friend WithEvents btnStart As Button
    Friend WithEvents btnReturnToMenu As Button
    Friend WithEvents btnAddPlayer As Button
    Friend WithEvents btnRmPlayer As Button
End Class
