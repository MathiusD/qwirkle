﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Settings
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_boardsizey = New System.Windows.Forms.Label()
        Me.lbl_boardsizex = New System.Windows.Forms.Label()
        Me.lbl_nbcolumn = New System.Windows.Forms.Label()
        Me.lbl_nbline = New System.Windows.Forms.Label()
        Me.numupdown_boardsizey = New System.Windows.Forms.NumericUpDown()
        Me.numupdown_boardsizex = New System.Windows.Forms.NumericUpDown()
        Me.numupdown_nbcolumndisplay = New System.Windows.Forms.NumericUpDown()
        Me.numupdown_nblinedisplay = New System.Windows.Forms.NumericUpDown()
        Me.btn_return = New System.Windows.Forms.Button()
        CType(Me.numupdown_boardsizey, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numupdown_boardsizex, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numupdown_nbcolumndisplay, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numupdown_nblinedisplay, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbl_boardsizey
        '
        Me.lbl_boardsizey.AutoSize = True
        Me.lbl_boardsizey.Location = New System.Drawing.Point(10, 10)
        Me.lbl_boardsizey.Name = "lbl_boardsizey"
        Me.lbl_boardsizey.Size = New System.Drawing.Size(122, 13)
        Me.lbl_boardsizey.TabIndex = 0
        Me.lbl_boardsizey.Text = "Horizontal Size Of Board"
        '
        'lbl_boardsizex
        '
        Me.lbl_boardsizex.AutoSize = True
        Me.lbl_boardsizex.Location = New System.Drawing.Point(10, 35)
        Me.lbl_boardsizex.Name = "lbl_boardsizex"
        Me.lbl_boardsizex.Size = New System.Drawing.Size(110, 13)
        Me.lbl_boardsizex.TabIndex = 1
        Me.lbl_boardsizex.Text = "Vertical Size Of Board"
        '
        'lbl_nbcolumn
        '
        Me.lbl_nbcolumn.AutoSize = True
        Me.lbl_nbcolumn.Location = New System.Drawing.Point(10, 60)
        Me.lbl_nbcolumn.Name = "lbl_nbcolumn"
        Me.lbl_nbcolumn.Size = New System.Drawing.Size(94, 13)
        Me.lbl_nbcolumn.TabIndex = 2
        Me.lbl_nbcolumn.Text = "Number of Column"
        '
        'lbl_nbline
        '
        Me.lbl_nbline.AutoSize = True
        Me.lbl_nbline.Location = New System.Drawing.Point(10, 85)
        Me.lbl_nbline.Name = "lbl_nbline"
        Me.lbl_nbline.Size = New System.Drawing.Size(79, 13)
        Me.lbl_nbline.TabIndex = 3
        Me.lbl_nbline.Text = "Number of Line"
        '
        'numupdown_boardsizey
        '
        Me.numupdown_boardsizey.Location = New System.Drawing.Point(150, 10)
        Me.numupdown_boardsizey.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.numupdown_boardsizey.Name = "numupdown_boardsizey"
        Me.numupdown_boardsizey.Size = New System.Drawing.Size(120, 20)
        Me.numupdown_boardsizey.TabIndex = 4
        Me.numupdown_boardsizey.Value = New Decimal(New Integer() {150, 0, 0, 0})
        '
        'numupdown_boardsizex
        '
        Me.numupdown_boardsizex.Location = New System.Drawing.Point(150, 35)
        Me.numupdown_boardsizex.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.numupdown_boardsizex.Name = "numupdown_boardsizex"
        Me.numupdown_boardsizex.Size = New System.Drawing.Size(120, 20)
        Me.numupdown_boardsizex.TabIndex = 5
        Me.numupdown_boardsizex.Value = New Decimal(New Integer() {150, 0, 0, 0})
        '
        'numupdown_nbcolumndisplay
        '
        Me.numupdown_nbcolumndisplay.Location = New System.Drawing.Point(150, 60)
        Me.numupdown_nbcolumndisplay.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.numupdown_nbcolumndisplay.Name = "numupdown_nbcolumndisplay"
        Me.numupdown_nbcolumndisplay.Size = New System.Drawing.Size(120, 20)
        Me.numupdown_nbcolumndisplay.TabIndex = 6
        Me.numupdown_nbcolumndisplay.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'numupdown_nblinedisplay
        '
        Me.numupdown_nblinedisplay.Location = New System.Drawing.Point(150, 85)
        Me.numupdown_nblinedisplay.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.numupdown_nblinedisplay.Name = "numupdown_nblinedisplay"
        Me.numupdown_nblinedisplay.Size = New System.Drawing.Size(120, 20)
        Me.numupdown_nblinedisplay.TabIndex = 7
        Me.numupdown_nblinedisplay.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'btn_return
        '
        Me.btn_return.Location = New System.Drawing.Point(250, 110)
        Me.btn_return.Name = "btn_return"
        Me.btn_return.Size = New System.Drawing.Size(130, 23)
        Me.btn_return.TabIndex = 8
        Me.btn_return.Text = "Return"
        Me.btn_return.UseVisualStyleBackColor = True
        '
        'Settings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 162)
        Me.ControlBox = False
        Me.Controls.Add(Me.btn_return)
        Me.Controls.Add(Me.numupdown_nblinedisplay)
        Me.Controls.Add(Me.numupdown_nbcolumndisplay)
        Me.Controls.Add(Me.numupdown_boardsizex)
        Me.Controls.Add(Me.numupdown_boardsizey)
        Me.Controls.Add(Me.lbl_nbline)
        Me.Controls.Add(Me.lbl_nbcolumn)
        Me.Controls.Add(Me.lbl_boardsizex)
        Me.Controls.Add(Me.lbl_boardsizey)
        Me.Name = "Settings"
        Me.Text = "Qwirkle - Settings"
        CType(Me.numupdown_boardsizey, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numupdown_boardsizex, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numupdown_nbcolumndisplay, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numupdown_nblinedisplay, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()
        Me.CenterToScreen()

    End Sub

    Friend WithEvents lbl_boardsizey As Label
    Friend WithEvents lbl_boardsizex As Label
    Friend WithEvents lbl_nbcolumn As Label
    Friend WithEvents lbl_nbline As Label
    Friend WithEvents numupdown_boardsizey As NumericUpDown
    Friend WithEvents numupdown_boardsizex As NumericUpDown
    Friend WithEvents numupdown_nbcolumndisplay As NumericUpDown
    Friend WithEvents numupdown_nblinedisplay As NumericUpDown
    Friend WithEvents btn_return As Button
End Class
