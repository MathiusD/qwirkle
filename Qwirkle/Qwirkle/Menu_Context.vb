﻿Imports ClassQwirkle
Imports Qwirkle.Exceptions
Public Class Menu_Context
    Dim Give_Up As List(Of Player) = New List(Of Player)
    Private Sub Menu_Context(sender As Object, e As KeyEventArgs) Handles Me.KeyDown, btn_exit.KeyDown, btn_return.KeyDown, lbl_title.KeyDown, btn_settings.KeyDown
        'Procédure permettant de fermer le menu contextuel
        'lors de l'appui de la touche 'Echap' et de réactiver
        'le formulaire Game.
        If e.KeyCode.Equals(Keys.Escape) Then
            Call btn_return_Click()
        End If
    End Sub
    Private Sub btn_return_Click() Handles btn_return.Click
        'Procéudre permettant de revenir au jeu.
        Qwirkle.Game.Enabled = True
        Qwirkle.Game.Show()
        Me.Hide()
    End Sub
    Private Sub btn_exit_Click() Handles btn_exit.Click
        'Procédure de fermeture de l'application.
        Qwirkle.Menu.Close()
        Qwirkle.Game.Close()
        Qwirkle.Context.Close()
        Call Reset()
        Me.Close()
    End Sub
    Private Sub btn_restart_Click() Handles btn_restart.Click
        'Procédure qui réinitialise le board et les joueurs
        'mais en conservant le même nombre de joueurs et avec
        'les mêmes noms et les mêmes couleurs.
        Dim players As List(Of Player) = New List(Of Player)()
        For i = 0 To Context.GetPlayers().Count - 1
            Dim player As Player = New Player(Context.GetPlayers()(i).getColor(), Context.GetPlayers()(i).getName(), Context.GetPlayers()(i).getAge())
            players.Add(player)
        Next
        Qwirkle.Context.SetPlayers(players)
        Qwirkle.Game.Reset()
        Qwirkle.Game.Game_Load()
        Qwirkle.Game.Enabled = True
        Me.Close()
    End Sub
    Private Sub btn_giveup_Click() Handles btn_giveup.Click
        'Procédure permetant l'abandon de la part d'un joueur
        'cela permet donc de passer automatiquement le tour
        'du joueur en question tout en replaçant ses tuiles
        'dans le sac.
        Dim B As Board = Qwirkle.Game.GetBoard()
        Give_Up.Add(B.getPlayers()(0))
        Qwirkle.Game.EndTurn()
        For Each tile As Tile In B.getPlayers()(B.getPlayers().Count - 1).getRack()
            B.AddStack(tile)
        Next
        If (B.getPlayers().Count - Give_Up.Count) <> 1 Then
            Qwirkle.Game.Enabled = True
            Qwirkle.Game.Show()
        Else
            Qwirkle.Game.Enabled = True
            Qwirkle.Game.Enabled = False
        End If
        Me.Hide()
    End Sub
    Public Sub Reset()
        'Procédure du reset du formulaire.
        Give_Up = New List(Of Player)
    End Sub
    Public Function GetGive_Up() As List(Of Player)
        'Accesseur de la liste des joueurs ayant
        'signifié leur abandon.
        Return Give_Up
    End Function
    Private Sub btn_settings_Click() Handles btn_settings.Click
        Me.Enabled = False
        Settings.MàJ()
        Settings.Show()
    End Sub
End Class