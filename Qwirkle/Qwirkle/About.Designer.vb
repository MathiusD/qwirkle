﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class About
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_author = New System.Windows.Forms.Label()
        Me.lbl_title = New System.Windows.Forms.Label()
        Me.btn_menu = New System.Windows.Forms.Button()
        Me.lbl_comment = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lbl_author
        '
        Me.lbl_author.AutoSize = True
        Me.lbl_author.Location = New System.Drawing.Point(25, 55)
        Me.lbl_author.Name = "lbl_author"
        Me.lbl_author.Size = New System.Drawing.Size(200, 13)
        Me.lbl_author.TabIndex = 3
        Me.lbl_author.Text = "Authors: Mathieu, Jason, Florian et Alexis"
        '
        'lbl_title
        '
        Me.lbl_title.AutoSize = True
        Me.lbl_title.Location = New System.Drawing.Point(70, 15)
        Me.lbl_title.Name = "lbl_title"
        Me.lbl_title.Size = New System.Drawing.Size(78, 13)
        Me.lbl_title.TabIndex = 2
        Me.lbl_title.Text = "Project Qwirkle"
        '
        'btn_menu
        '
        Me.btn_menu.Location = New System.Drawing.Point(60, 75)
        Me.btn_menu.Name = "btn_menu"
        Me.btn_menu.Size = New System.Drawing.Size(100, 25)
        Me.btn_menu.TabIndex = 0
        Me.btn_menu.Text = "Return To Menu"
        Me.btn_menu.UseVisualStyleBackColor = True
        '
        'lbl_comment
        '
        Me.lbl_comment.AutoSize = True
        Me.lbl_comment.Location = New System.Drawing.Point(85, 35)
        Me.lbl_comment.Name = "lbl_comment"
        Me.lbl_comment.Size = New System.Drawing.Size(46, 13)
        Me.lbl_comment.TabIndex = 4
        Me.lbl_comment.Text = "Thanks."
        '
        'About
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(234, 112)
        Me.ControlBox = False
        Me.Controls.Add(Me.lbl_comment)
        Me.Controls.Add(Me.btn_menu)
        Me.Controls.Add(Me.lbl_title)
        Me.Controls.Add(Me.lbl_author)
        Me.Name = "About"
        Me.Text = "Qwirkle - About"
        Me.ResumeLayout(False)
        Me.PerformLayout()
        Me.CenterToScreen()
    End Sub

    Friend WithEvents lbl_author As Label
    Friend WithEvents lbl_title As Label
    Friend WithEvents btn_menu As Button
    Friend WithEvents lbl_comment As Label
End Class
