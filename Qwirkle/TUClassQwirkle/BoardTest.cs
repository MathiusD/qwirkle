﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassQwirkle;
using Qwirkle.Exceptions;

namespace TUClassQwirkle
{
    [TestClass]
    public class BoardTest
    {
        /*
        Tests Associés à la Classe Board.
        */
        [TestMethod]
        public void ConstructorTest()
        {
            /*
            Test du Constructeur de Board.
            Le but de ce Test est de savoir si une exception est relevée
            lors de la création de celui-ci.
            */
            Player J1 = new Player("white", "Eudes", 0);
            Player J2 = new Player("black", "Eude", 0);
            Player J3 = new Player("blue", "eude", 0);
            Player J4 = new Player("green", "eudes", 0);
            Player J5 = new Player("green", "udes", 0);
            List<Player> LJ1 = new List<Player>();
            List<Player> LJ2 = new List<Player>();
            LJ1.Add(J1);
            bool test = false;
            try
            {
                Board B2 = new Board(LJ1);
            }
            catch (Exception E)
            {
                if (E.GetType().IsInstanceOfType(new WrongNumberOfPlayerException()))
                {
                    if (E.Message == "Wrong number of players. You have entered 1 Players, excluding 1 is not between 2 and 4.")
                    {
                        test = true;
                    }
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
            LJ1.Add(J2);
            LJ1.Add(J3);
            LJ1.Add(J4);
            LJ1.Add(J5);
            test = false;
            try
            {
                Board B2 = new Board(LJ1);
            }
            catch (Exception E)
            {
                if (E.GetType().IsInstanceOfType(new WrongNumberOfPlayerException()))
                {
                    if (E.Message == "Wrong number of players. You have entered 5 Players, excluding 5 is not between 2 and 4.")
                    {
                        test = true;
                    }
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
            LJ2.Add(J1);
            LJ2.Add(J2);
            LJ2.Add(J3);
            Board B1 = new Board(LJ2);
        }
        [TestMethod]
        public void GetStackTest()
        {
            /*
            Test de l'accesseur associé à la variable stack.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien le stack aux
            bonnes valeurs.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Player J2 = new Player("black", "Eude", 2);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            List<Tile> LT1 = new List<Tile>();
            for (short i = 0; i < 3; i++)
            {
                for (short j = 0; j < 6; j++)
                {
                    for (short k = 0; k < 6; k++)
                    {
                        Tile tile = new Tile(i, j, k);
                        LT1.Add(tile);
                    }
                }
            }
            for (short j = 0; j < J1.getRack().Count; j++)
            {
                LT1.Remove(J1.getRack()[j]);
                LT1.Remove(J2.getRack()[j]);
            }
            Assert.AreEqual(true, LT1.Count == B1.getStack().Count);
            for (short l = 0; l < B1.getStack().Count; l++)
            {
                short m = 0;
                while ((m < LT1.Count - 1) && (!(LT1[m].Equals(B1.getStack()[l]))))
                {
                    m = (short)(m + 1);
                }
                Assert.AreEqual(true, LT1[m].Equals(B1.getStack()[l]));
            }
        }
        [TestMethod]
        public void GetNbColumnTest()
        {
            /*
            Test de l'accesseur associé à la variable nbcolumn.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien le stack aux
            bonnes valeurs.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Player J2 = new Player("black", "Eude", 2);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Assert.AreEqual(1, B1.getNbColumn());
        }
        [TestMethod]
        public void GetNbLineTest()
        {
            /*
            Test de l'accesseur associé à la variable nbline.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien le stack aux
            bonnes valeurs.
            */
            Player J1 = new Player("white", "Eudes", 0);
            Player J2 = new Player("black", "Eude", 2);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Assert.AreEqual(1, B1.getNbLine());
        }
        [TestMethod]
        public void GetBoardTest()
        {
            /*
            Test de l'accesseur associé à la variable board.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien le stack aux
            bonnes valeurs.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Player J2 = new Player("black", "Eude", 2);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            List<List<Tile>> LT1 = new List<List<Tile>>(B1.getNbLine());
            for (short i = 0; i < B1.getNbLine(); i++)
            {
                List<Tile> Line = new List<Tile>(B1.getNbColumn());
                LT1.Add(Line);
                for (short f = 0; f < B1.getNbColumn(); f++)
                {
                    Tile tilenull = new Tile();
                    LT1[i].Add(tilenull);
                }
            }
            Assert.AreEqual(true, LT1.Count == B1.getBoard().Count);
            for (short i = 0; i < B1.getBoard().Count; i++)
            {
                Assert.AreEqual(true, LT1[i].Count == B1.getBoard()[i].Count);
                for (short j = 0; j < B1.getBoard()[i].Count; j++)
                {
                    short k = 0;
                    while ((k < LT1[i].Count - 1) && (!(LT1[i][k].Equals(B1.getBoard()[i][j]))))
                    {
                        k = (short)(k + 1);
                    }
                    Assert.AreEqual(true, LT1[i][k].Equals(B1.getBoard()[i][j]));
                }
            }
        }
        [TestMethod]
        public void GetBoardSizeXTest()
        {
            /*
            Test de l'accesseur associé à la variable boardsizex.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien le stack aux
            bonnes valeurs.
            */
            Player J1 = new Player("white", "Eudes", 2);
            Player J2 = new Player("black", "Eude", 2);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Assert.AreEqual(400, B1.getBoardSizeX());
        }
        [TestMethod]
        public void GetBoardSizeYTest()
        {
            /*
            Test de l'accesseur associé à la variable boardsizey.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien le stack aux
            bonnes valeurs.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Player J2 = new Player("black", "Eude", 1);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Assert.AreEqual(400, B1.getBoardSizeY());
        }
        [TestMethod]
        public void GetTileizeXTest()
        {
            /*
            Test de l'accesseur associé à la variable tilesizex.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien le stack aux
            bonnes valeurs.
            */
            Player J1 = new Player("white", "Eudes", 3);
            Player J2 = new Player("black", "Eude", 42);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Assert.AreEqual(40, B1.getTileSizeX());
        }
        [TestMethod]
        public void GetTileizeYTest()
        {
            /*
            Test de l'accesseur associé à la variable tilesizey.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien le stack aux
            bonnes valeurs.
            */
            Player J1 = new Player("white", "Eudes", 0);
            Player J2 = new Player("black", "Eude", 1);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Assert.AreEqual(40, B1.getTileSizeY());
        }
        [TestMethod]
        public void GetPlaced_TileTest()
        {
            /*
            Test de l'accesseur associé à la variable placed_tile.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien le stack aux
            bonnes valeurs.
            */
            Player J1 = new Player("white", "Eudes", 0);
            Player J2 = new Player("black", "Eude", 1);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            List<Tile> LT1 = new List<Tile>();
            Assert.AreEqual(true, LT1.Count == B1.getPlaced_Tiles().Count);
            for (short i = 0; i < B1.getPlaced_Tiles().Count; i++)
            {
                short j = 0;
                while ((j < LT1.Count - 1) && (!(LT1[j].Equals(B1.getPlaced_Tiles()[i]))))
                {
                    j = (short)(j + 1);
                }
                Assert.AreEqual(true, LT1[j].Equals(B1.getPlaced_Tiles()[i]));
            }
        }
        [TestMethod]
        public void GetPlayerTest()
        {
            /*
            Test de l'accesseur associé à la variable players.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien le stack aux
            bonnes valeurs.
            */
            Player J1 = new Player("white", "Eudes", 3);
            Player J2 = new Player("black", "Eude", 6);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Assert.AreEqual(true, LJ1.Count == B1.getPlayers().Count);
            for (short i = 0; i < B1.getPlayers().Count; i++)
            {
                short j = 0;
                while ((j < LJ1.Count - 1) && (!(LJ1[j].Equals(B1.getPlayers()[i]))))
                {
                    j = (short)(j + 1);
                }
                Assert.AreEqual(true, LJ1[j].Equals(B1.getPlayers()[i]));
            }
        }
        [TestMethod]
        public void SetNbColumnTest()
        {
            /*
            Test de la méthode de redéfinition associé à la variable
            nbcolumn.
            Le but de vérifier que la redéfinition est correctement
            appliquée.
            */
            Player J1 = new Player("white", "Eudes", 78);
            Player J2 = new Player("black", "Eude", 9);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Assert.AreEqual(1, B1.getNbColumn());
            B1.setNbColumn(32);
            Assert.AreEqual(32, B1.getNbColumn());
            List<List<Tile>> LT1 = new List<List<Tile>>(B1.getNbLine());
            for (short i = 0; i < B1.getNbLine(); i++)
            {
                List<Tile> Line = new List<Tile>(B1.getNbColumn());
                LT1.Add(Line);
                for (short f = 0; f < B1.getNbColumn(); f++)
                {
                    Tile tilenull = new Tile();
                    LT1[i].Add(tilenull);
                }
            }
            Assert.AreEqual(true, LT1.Count == B1.getBoard().Count);
            for (short i = 0; i < B1.getBoard().Count; i++)
            {
                Assert.AreEqual(true, LT1[i].Count == B1.getBoard()[i].Count);
                for (short j = 0; j < B1.getBoard()[i].Count; j++)
                {
                    short k = 0;
                    while ((k < LT1[i].Count - 1) && (!(LT1[i][k].Equals(B1.getBoard()[i][j]))))
                    {
                        k = (short)(k + 1);
                    }
                    Assert.AreEqual(true, LT1[i][k].Equals(B1.getBoard()[i][j]));
                }
            }
            Tile TileA = new Tile(0, 0, 0);
            B1.AddBoard(TileA, 0, 0);
            bool test = false;
            try
            {
                B1.setNbColumn(32);
            }
            catch (Exception E)
            {
                if (E.GetType().IsInstanceOfType(new BoardIsNotEmptyException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void SetNbLineTest()
        {
            /*
            Test de la méthode de redéfinition associé à la variable
            nbline.
            Le but de vérifier que la redéfinition est correctement
            appliquée.
            */
            Player J1 = new Player("white", "Eudes", 2);
            Player J2 = new Player("black", "Eude", 3);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Assert.AreEqual(1, B1.getNbLine());
            B1.setNbLine(32);
            Assert.AreEqual(32, B1.getNbLine());
            List<List<Tile>> LT1 = new List<List<Tile>>(B1.getNbLine());
            for (short i = 0; i < B1.getNbLine(); i++)
            {
                List<Tile> Line = new List<Tile>(B1.getNbColumn());
                LT1.Add(Line);
                for (short f = 0; f < B1.getNbColumn(); f++)
                {
                    Tile tilenull = new Tile();
                    LT1[i].Add(tilenull);
                }
            }
            Assert.AreEqual(true, LT1.Count == B1.getBoard().Count);
            for (short i = 0; i < B1.getBoard().Count; i++)
            {
                Assert.AreEqual(true, LT1[i].Count == B1.getBoard()[i].Count);
                for (short j = 0; j < B1.getBoard()[i].Count; j++)
                {
                    short k = 0;
                    while ((k < LT1[i].Count - 1) && (!(LT1[i][k].Equals(B1.getBoard()[i][j]))))
                    {
                        k = (short)(k + 1);
                    }
                    Assert.AreEqual(true, LT1[i][k].Equals(B1.getBoard()[i][j]));
                }
            }
            Tile TileA = new Tile(0, 0, 0);
            B1.AddBoard(TileA, 0, 0);
            bool test = false;
            try
            {
                B1.setNbLine(32);
            }
            catch (Exception E)
            {
                if (E.GetType().IsInstanceOfType(new BoardIsNotEmptyException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void SetNbColumnDisplayTest()
        {
            /*
            Test de la méthode de redéfinition associé à la variable
            nblinedisplay.
            Le but de vérifier que la redéfinition est correctement
            appliquée.
            */
            Player J1 = new Player("white", "Eudes", 7);
            Player J2 = new Player("black", "Eude", 9);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Assert.AreEqual(10, B1.getNbColumnDisplay());
            B1.setNbColumnDisplay(20);
            Assert.AreEqual(20, B1.getNbColumnDisplay());
            Assert.AreEqual(20, B1.getTileSizeY());
        }
        [TestMethod]
        public void SetNbLineDisplayTest()
        {
            /*
            Test de la méthode de redéfinition associé à la variable
            nblinedisplay.
            Le but de vérifier que la redéfinition est correctement
            appliquée.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Player J2 = new Player("black", "Eude", 3);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Assert.AreEqual(10, B1.getNbLineDisplay());
            B1.setNbLineDisplay(20);
            Assert.AreEqual(20, B1.getNbLineDisplay());
            Assert.AreEqual(20, B1.getTileSizeX());
        }
        [TestMethod]
        public void SetBoardSizeXTest()
        {
            /*
            Test de la méthode de redéfinition associé à la variable
            boardsizex.
            Le but de vérifier que la redéfinition est correctement
            appliquée.
            */
            Player J1 = new Player("white", "Eudes", 4);
            Player J2 = new Player("black", "Eude", 7);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Assert.AreEqual(400, B1.getBoardSizeX());
            B1.setBoardSizeX(800);
            Assert.AreEqual(800, B1.getBoardSizeX());
            Assert.AreEqual(80, B1.getTileSizeX());
        }
        [TestMethod]
        public void SetBoardSizeYTest()
        {
            /*
            Test de la méthode de redéfinition associé à la variable
            boardsizey.
            Le but de vérifier que la redéfinition est correctement
            appliquée.
            */
            Player J1 = new Player("white", "Eudes", 3);
            Player J2 = new Player("black", "Eude", 4);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Assert.AreEqual(400, B1.getBoardSizeY());
            B1.setBoardSizeY(800);
            Assert.AreEqual(800, B1.getBoardSizeY());
            Assert.AreEqual(80, B1.getTileSizeY());
        }
        [TestMethod]
        public void AddBoardTest()
        {
            /*
            Test de la méthode AddBoard().
            Le but est de vérifier que cela permet effectivement
            d'ajouter une tuile au board sans erreur.
            On vérifie également que la méthode renvoie effectivement
            des Exceptions lorsqu'on tente de supplanter une tuile
            déjà présente ou lorsqu'on tente de placer une tuile
            hors du plateau.
            */
            Player J1 = new Player("white", "Eudes", 6);
            Player J2 = new Player("black", "Eude", 7);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Tile TileA = new Tile(0, 4, 0);
            B1.AddBoard(TileA, 0, 0);
            List<List<Tile>> LT1 = new List<List<Tile>>(B1.getNbLine());
            for (short i = 0; i < B1.getNbLine(); i++)
            {
                List<Tile> Line = new List<Tile>(B1.getNbColumn());
                LT1.Add(Line);
                for (short f = 0; f < B1.getNbColumn(); f++)
                {
                    Tile tilenull = new Tile();
                    LT1[i].Add(tilenull);
                }
            }
            LT1[1][1] = TileA;
            Assert.AreEqual(LT1.Count, B1.getBoard().Count);
            for (short i = 0; LT1.Count > i; i++)
            {
                Assert.AreEqual(LT1[i].Count, B1.getBoard()[i].Count);
                if (LT1[i].Count > 0)
                {
                    for (short j = 0; LT1[i].Count > j; j++)
                    {
                        Assert.AreEqual(LT1[i][j], B1.getBoard()[i][j]);
                    }
                }
            }
            Tile TileB = new Tile(2, 4, 3);
            B1.AddBoard(TileB, (short) (B1.getNbLine() - 1), (short) (B1.getNbColumn() - 1));
            List<List<Tile>> LT3 = new List<List<Tile>>(B1.getNbLine());
            for (short i = 0; i < B1.getNbLine(); i++)
            {
                List<Tile> Line = new List<Tile>(B1.getNbColumn());
                LT3.Add(Line);
                for (short f = 0; f < B1.getNbColumn(); f++)
                {
                    Tile tilenull = new Tile();
                    LT3[i].Add(tilenull);
                }
            }
            LT3[1][1] = TileA;
            LT3[B1.getNbLine() - 2][B1.getNbColumn() - 2] = TileB;
            Assert.AreEqual(LT3.Count, B1.getBoard().Count);
            for (short i = 0; LT3.Count > i; i++)
            {
                Assert.AreEqual(LT3[i].Count, B1.getBoard()[i].Count);
                if (LT3[i].Count > 0)
                {
                    for (short j = 0; LT3[i].Count > j; j++)
                    {
                        Assert.AreEqual(LT3[i][j], B1.getBoard()[i][j]);
                    }
                }
            }
            Board B2 = new Board(LJ1);
            B2.AddBoard(TileA, 0, 0);
            List<List<Tile>> LT2 = new List<List<Tile>>(B2.getNbLine());
            for (short i = 0; i < B2.getNbLine(); i++)
            {
                List<Tile> Line = new List<Tile>(B2.getNbColumn());
                LT2.Add(Line);
                for (short f = 0; f < B2.getNbColumn(); f++)
                {
                    Tile tilenull = new Tile();
                    LT2[i].Add(tilenull);
                }
            }
            LT2[1][1] = TileA;
            Assert.AreEqual(LT2.Count, B2.getBoard().Count);
            for (short i = 0; LT2.Count > i; i++)
            {
                Assert.AreEqual(LT2[i].Count, B2.getBoard()[i].Count);
                if (LT2[i].Count > 0)
                {
                    for (short j = 0; LT2[i].Count > j; j++)
                    {
                        Assert.AreEqual(LT2[i][j], B2.getBoard()[i][j]);
                    }
                }
            }
            bool test = false;
            try
            {
                B2.AddBoard(TileB, 1, 1);
            }
            catch (Exception E)
            {
                if (E.GetType().IsInstanceOfType(new WrongPositionForTileException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
            test = false;
            try
            {
                B2.AddBoard(TileB, -1, 2);
            }
            catch (Exception E)
            {
                if (E.GetType().IsInstanceOfType(new TileOutOfLimitsException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void AddStackTest()
        {
            /*
            Test de la méthode AddStack().
            Le but est de vérifier que cela permet effectivement
            d'ajouter une tuile au stack sans erreur.
            */
            Player J1 = new Player("white", "Eudes", 7);
            Player J2 = new Player("black", "Eude", 6);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Tile TileA = new Tile();
            B1.AddStack(TileA);
            List<Tile> LT1 = new List<Tile>();
            LT1.Add(TileA);
            for (short i = 0; i < 3; i++)
            {
                for (short j = 0; j < 6; j++)
                {
                    for (short k = 0; k < 6; k++)
                    {
                        Tile tile = new Tile(i, j, k);
                        LT1.Add(tile);
                    }
                }
            }
            for (short j = 0; j < J1.getRack().Count; j++)
            {
                LT1.Remove(J1.getRack()[j]);
                LT1.Remove(J2.getRack()[j]);
            }
            Assert.AreEqual(true, LT1.Count == B1.getStack().Count);
            for (short l = 0; l < B1.getStack().Count; l++)
            {
                short m = 0;
                while ((m < LT1.Count - 1) && (!(LT1[m].Equals(B1.getStack()[l]))))
                {
                    m = (short)(m + 1);
                }
                Assert.AreEqual(true, LT1[m].Equals(B1.getStack()[l]));
            }
        }
        [TestMethod]
        public void RemoveStackTest()
        {
            /*
            Test de la méthode RemoveStack().
            Le but est de vérifier que cela permet effectivement
            de retirer une tuile au stack sans erreur et que si
            l'on tente d'enlever une tuile non présente dans le
            stack, on vérifie que la méthode nous renvoie bien
            une Exception de type RemoveWrongTilesException.
            */
            Player J1 = new Player("white", "Eudes", 0);
            Player J2 = new Player("black", "Eude", 1);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Random r = new Random();
            int index = r.Next(B1.getStack().Count - 1);
            B1.RemoveStack(B1.getStack()[index]);
            List<Tile> LT1 = new List<Tile>();
            for (short i = 0; i < 3; i++)
            {
                for (short j = 0; j < 6; j++)
                {
                    for (short k = 0; k < 6; k++)
                    {
                        Tile tile = new Tile(i, j, k);
                        LT1.Add(tile);
                    }
                }
            }
            for (short j = 0; j < J1.getRack().Count; j++)
            {
                LT1.Remove(J1.getRack()[j]);
                LT1.Remove(J2.getRack()[j]);
            }
            LT1.Remove(LT1[index]);
            Assert.AreEqual(true, LT1.Count == B1.getStack().Count);
            for (short l = 0; l < B1.getStack().Count; l++)
            {
                short m = 0;
                while ((m < LT1.Count - 1) && (!(LT1[m].Equals(B1.getStack()[l]))))
                {
                    m = (short)(m + 1);
                }
                Assert.AreEqual(true, LT1[m].Equals(B1.getStack()[l]));
            }
            bool test = false;
            try
            {
                Tile TileNULL = new Tile();
                B1.RemoveStack(TileNULL);
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new RemoveWrongTilesException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void Is_Stack_EmptyTest()
        {
            /*
            Test de la méthode Is_Stack_Empty().
            Le but est de vérifier que cela permet effectivement de
            retourner un booléen indiquant si le rack est effectivement
            vide.
            */
            Player J1 = new Player("white", "Eudes", 3);
            Player J2 = new Player("black", "Eude", 0);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Assert.AreEqual(false, B1.Is_Stack_Empty());
        }
        [TestMethod]
        public void Player_ExistTest()
        {
            /*
            Test de la méthode Player_Exist().
            Le but est de vérifier que cela permet effectivement de
            retourner un booléen indiquant si le joueur est présent
            au sein du board depuis lequel est appelé la méthode.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Player J2 = new Player("white", "Eude", 0);
            Player J3 = new Player("black", "Eudes", 3);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J3);
            Board B1 = new Board(LJ1);
            Assert.AreEqual(true, B1.Player_Exist(J1));
            Assert.AreEqual(false, B1.Player_Exist(J2));
        }
        [TestMethod]
        public void Board_SearchTest()
        {
            /*
            Test de la méthode Board_Search().
            Le but est de vérifier que la méthode renvoie bien les
            coordonnées de la tuile passée en paramètre ou 
            renvoie une Exception de Type TileDoes'ntExistException
            dans le cas où la tuile passé en paramètre n'existe pas
            au sein du board en question.
            */
            Player J1 = new Player("white", "Eudes", 3);
            Player J2 = new Player("black", "Eude", 1);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Tile TileA = new Tile(0, 4, 0);
            Tile TileB = new Tile(1, 4, 0);
            B1.AddBoard(TileA, 0, 0);
            int[] Coord1 = new int[2];
            Coord1[0] = 1;
            Coord1[1] = 1;
            Assert.AreEqual(Coord1[0], B1.Board_Search(TileA)[0]);
            Assert.AreEqual(Coord1[1], B1.Board_Search(TileA)[1]);
            bool test = false;
            try
            {
                B1.Board_Search(TileB);
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new TileDoes_ntExistException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
            B1.AddBoard(TileB, 0, 0);
            int[] Coord2 = new int[2];
            int[] Coord3 = new int[2];
            Coord2[0] = 1;
            Coord2[1] = 1;
            Coord3 = B1.Board_Search(TileB);
            Assert.AreEqual(Coord2[0], Coord3[0]);
            Assert.AreEqual(Coord2[1], Coord3[1]);

        }
        [TestMethod]
        public void FirstTileTest()
        {
            /*
            Test de la méthode FirstTile().
            Le but est de vérifier que la méthode renvoie bien 
            le booléen en fonction de l'état du board.
            */
            Player J1 = new Player("white", "Eudes", 2);
            Player J2 = new Player("black", "Eude", 0);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Tile TileA = new Tile(0, 4, 0);
            Tile TileB = new Tile(1, 4, 0);
            Assert.AreEqual(true, B1.FirstTile());
            B1.AddBoard(TileA, 0, 0);
            Assert.AreEqual(false, B1.FirstTile());
        }
        [TestMethod]
        public void NbTileTest()
        {
            /*
            Test de la méthode NbTile().
            Le but est de vérifier que la méthode renvoie bien 
            un short qui indique le nombre de tuile placée sur
            le board.
            */
            Player J1 = new Player("white", "Eudes", 0);
            Player J2 = new Player("black", "Eude", 1);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Tile TileA = new Tile(0, 4, 0);
            Tile TileB = new Tile(1, 4, 0);
            Assert.AreEqual(0, B1.NbTile());
            B1.AddBoard(TileA, 0, 0);
            Assert.AreEqual(1, B1.NbTile());
            B1.AddBoard(TileB, 1, 0);
            Assert.AreEqual(2, B1.NbTile());
        }
        [TestMethod]
        public void Calcul_ScoreTest()
        {
            /*
            Test de la méthode Calcul_Score().
            */
            Player J1 = new Player("white", "Eudes", 1);
            Player J2 = new Player("black", "Eude", 0);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B3 = new Board(LJ1);
            Tile TileA = new Tile(1, 4, 2);
            Tile TileAbis = new Tile(0, 4, 2);
            Tile TileB = new Tile(2, 4, 3);
            Tile TileBbis = new Tile(1, 4, 3);
            Tile TileBter = new Tile(0, 4, 3);
            Tile TileC = new Tile(2, 4, 1);
            Tile TileCbis = new Tile(1, 4, 1);
            bool test = false;
            try
            {
                B3.Calcul_Score();
            } catch(Exception e)
            {
                if (e.GetType().IsInstanceOfType(new NoTilePlacedInThisTurnException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
            B3.AddBoard(TileA, 0, 0);
            B3.AddTile(TileAbis, 2, 1);
            Assert.AreEqual(2, B3.Calcul_Score());
            B3.AddTile(TileB, 3, 1);
            Assert.AreEqual(3, B3.Calcul_Score());
            B3.AddTile(TileBbis, 4, 1);
            Assert.AreEqual(4, B3.Calcul_Score());
            B3.AddTile(TileBter, 5, 1);
            Assert.AreEqual(5, B3.Calcul_Score());
            B3.AddTile(TileC, 6, 1);
            Assert.AreEqual(12, B3.Calcul_Score());
            B3.AddBoard(TileCbis, 5, 2);
            Assert.AreEqual(14, B3.Calcul_Score());
        }
        [TestMethod]
        public void Rules_VerificationTest()
        {
            /*
            Test de la méthode Rules_Verification().
            */
            Player J1 = new Player("white", "Eudes", 0);
            Player J2 = new Player("black", "Eude", 1);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);

            Tile T1 = new Tile(1, 1, 1);
            Tile T2 = new Tile(2, 1, 2);
            Tile T3 = new Tile(2, 1, 3);
            Tile T4 = new Tile(2, 2, 2);
            Tile T5 = new Tile(2, 2, 2);
            bool test = false;
            try
            {
                B1.Rules_Verification(T1, 1, 1);
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new TileOutOfLimitsException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
            B1.AddBoard(T2, 0, 0);
            Assert.AreEqual(true, B1.Rules_Verification(T1, 2, 1));
            test = false;
            try
            {

                B1.Rules_Verification(T2, 1, 2);
            }
            catch (WrongPositionForTileException e)
            {
                if (e.GetComplement() == "The up tiles don't have only 1 similarity with this tile.")
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
            B1.AddBoard(T1, 1, 2);
            test = false;
            try
            {
                B1.Rules_Verification(T1, 1, 2); //T1 = T1
            }
            catch (WrongPositionForTileException e)
            {
                if (e.GetComplement() == "This place is already occupied.")
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
            test = false;
            try
            {
                B1.Rules_Verification(T4, 1, 2);
            }
            catch (WrongPositionForTileException e)
            {
                if (e.GetComplement() == "This place is already occupied.")
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
            test = false;
            try
            {
                B1.Rules_Verification(T3, 1, 2);

            }
            catch (WrongPositionForTileException e)
            {
                if (e.GetComplement() == "This place is already occupied.")
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
            test = false;
            try
            {
                B1.Rules_Verification(T5, 1, 2);
            }
            catch (WrongPositionForTileException e)
            {
                if (e.GetComplement() == "This place is already occupied.")
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void PossibilityOfPlayingTest()
        {
            /*
            Test de la méthode PossibilityOfPlaying().
            Le but est de vérifier que la méthode renvoie bien true
            ou false en fonction de si on peut jouer avec la liste
            de tuile passé en paramètres de la méthode.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Player J2 = new Player("black", "Eude", 0);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            List<Tile> LT1 = new List<Tile>();
            Assert.AreEqual(false, B1.PossibilityOfPlaying(LT1));
            LT1.AddRange(B1.getStack());
            LT1.AddRange(J1.getRack());
            LT1.AddRange(J2.getRack());
            Assert.AreEqual(true, B1.PossibilityOfPlaying(LT1));
        }
        [TestMethod]
        public void CancelPlacementTest()
        {
            /*
            Test de la méthode CancelPlacement().
            Le but est de vérifier que la méthode retire bien les tuiles
            placée et renvoie bien une exception si on tente de le faire
            'à vide'.
            */
            Player J1 = new Player("white", "Eudes", 3);
            Player J2 = new Player("black", "Eude", 7);
            Tile tilenull = new Tile();
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            bool test = false;
            try
            {
                B1.CancelPlacement();
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new NoTilePlacedInThisTurnException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
            List<Tile> R1 = new List<Tile>(); 
            for (short i = 0; i < B1.getPlayers()[0].getRack().Count; i++)
            {
                R1.Add(B1.getPlayers()[0].getRack()[i]);
            }
            B1.getPlayers()[0].Place_Tiles(B1, B1.getPlayers()[0].getRack()[0], 0, 0);
            B1.CancelPlacement();
            for (short i = 0; i < B1.getPlayers()[0].getRack().Count; i++)
            {
                short m = 0;
                while ((m < R1.Count - 1) && (!(R1[m].Equals(B1.getPlayers()[0].getRack()[i]))))
                {
                    m = (short)(m + 1);
                }
                Assert.AreEqual(R1[m], B1.getPlayers()[0].getRack()[i]);
            }
            Assert.AreEqual(0, B1.getPlaced_Tiles().Count);
            Assert.AreEqual(tilenull, B1.getBoard()[0][0]);
        }
        [TestMethod]
        public void ResizeBoardTest()
        {
            /*
            Test de la méthode ReziseBoard().
            Le but est de vérifier que le plateau se redimmensionne
            bien correctement en cas de besoin
            */
            Player J1 = new Player("white", "Eudes", 9);
            Player J2 = new Player("black", "Eude", 10);
            Tile tilenull = new Tile();
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            bool Test = true;
            Board B1;
            Tile tileA;
            do
            {
                bool Test2 = true;
                do
                {
                    B1 = new Board(LJ1);
                    tileA = B1.getPlayers()[0].getRack()[0];
                    B1.getPlayers()[0].Place_Tiles(B1, B1.getPlayers()[0].getRack()[0], 0, 0);
                    B1.EndTurn();
                    bool place1 = false;
                    short k = 0;
                    do
                    {
                        short x = 0;
                        do
                        {
                            short y = 0;
                            do
                            {
                                bool test = true;
                                try
                                {
                                    B1.getPlayers()[0].Place_Tiles(B1, B1.getPlayers()[0].getRack()[k], x, y);
                                }
                                catch (WrongPositionForTileException)
                                {
                                    test = false;
                                }
                                finally
                                {
                                    if (test == true)
                                    {
                                        place1 = true;
                                    }
                                }
                                y++;
                            } while(y < B1.getNbColumn() && place1 != true);
                            x++;
                        } while (x < B1.getNbLine() && place1 != true);
                        k++;
                    } while (k < B1.getPlayers()[0].getRack().Count && place1 != true);
                    if (place1 != true)
                    {
                        Test2 = false;
                    }
                } while (Test2 == false);
                bool place2 = false;
                short j = 0;
                do
                {
                    short x = 0;
                    do
                    {
                        short y = 0;
                        do
                        {
                            bool test = true;
                            try
                            {
                                B1.getPlayers()[0].Place_Tiles(B1, B1.getPlayers()[0].getRack()[j], x, y);
                            }
                            catch (WrongPositionForTileException)
                            {
                                test = false;
                            }
                            finally
                            {
                                if (test == true)
                                {
                                    place2 = true;
                                }
                            }
                            y++;
                        } while (y < B1.getNbColumn() && place2 != true);
                        x++;
                    } while (x < B1.getNbLine() && place2 != true);
                    j++;
                } while (j < B1.getPlayers()[0].getRack().Count && place2 != true);
                if (place2 != true)
                {
                    Test = false;
                }
            } while (Test == false);
            B1.CancelPlacement();
            Assert.AreEqual(3, B1.getNbLine());
            Assert.AreEqual(3, B1.getNbColumn());
            List<List<Tile>> LT1 = new List<List<Tile>>(3);
            for (short i = 0; i < 3; i++)
            {
                List<Tile> Line = new List<Tile>(3);
                LT1.Add(Line);
                for (short f = 0; f < 3; f++)
                {
                    Tile tilenull2 = new Tile();
                    LT1[i].Add(tilenull2);
                }
            }
            LT1[1][1] = tileA;
            Assert.AreEqual(true, LT1.Count == B1.getBoard().Count);
            for (short i = 0; i < B1.getBoard().Count; i++)
            {
                Assert.AreEqual(true, LT1[i].Count == B1.getBoard()[i].Count);
                for (short j = 0; j < B1.getBoard()[i].Count; j++)
                {
                    short k = 0;
                    while ((k < LT1[i].Count - 1) && (!(LT1[i][k].Equals(B1.getBoard()[i][j]))))
                    {
                        k = (short)(k + 1);
                    }
                    Assert.AreEqual(true, LT1[i][k].Equals(B1.getBoard()[i][j]));
                }
            }
        }
        [TestMethod]
        public void EndTurnTest()
        {
            /*
            Test de la méthode EndTurn().
            Le but est de vérifier que la méthode renvoie bien 0 si le
            tour se termine et que la partie se poursuit et qu'il renvoie
            bien 1 si la partie s'achève à la fin de se tour.
            */
            Player J1 = new Player("white", "Eudes", 0);
            Player J2 = new Player("white", "Eudes", 12);
            Player J3 = new Player("black", "Eude", 1);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J3);
            Board B1 = new Board(LJ1);
            if (B1.getPlayers()[0] == J1)
            {
                Assert.AreEqual(0, B1.EndTurn());
                Assert.AreEqual(J3, B1.getPlayers()[0]);
                Assert.AreEqual(J1, B1.getPlayers()[1]);
            }
            else
            {
                Assert.AreEqual(0, B1.EndTurn());
                Assert.AreEqual(J1, B1.getPlayers()[0]);
                Assert.AreEqual(J3, B1.getPlayers()[1]);
            }
        }
    }
}
