﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassQwirkle;
using Qwirkle.Exceptions;

namespace TUClassQwirkle
{
    [TestClass]
    public class TilesTest
    {
        /*
        Tests Associés à la Classe Tiles.
        */
        [TestMethod]
        public void ConstructorITest()
        {
            /*
            Test du Constructeur de Tuiles (sans Arguments).
            Le but de ce Test est de savoir si une exception est relevée
            lors de la création de celui-ci.
            */
            Tile TileA = new Tile();
        }
        [TestMethod]
        public void ConstructorIITest()
        {
            /*
            Test du Constructeur de Tuiles (avec Arguments).
            Le but de ce Test est de savoir si une exception est relevée
            lors de la création de celui-ci.
            On teste également si dans le cas où l'on fournit des arguments
            invalides, que le constructeur nous renvoie bien une Exception
            de type BadParametersForTilesConstructorException().
            */
            Tile TileA = new Tile(0, 0, 0);
            bool test = false;
            try
            {
                Tile TileB = new Tile(-4, 5, 4);
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new BadParametersForTileConstructorException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void GetIdTest()
        {
            /*
            Test de l'accesseur associé à la variable id.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien le id aux
            bonnes valeurs.
            */
            Tile TileA = new Tile(0, 0, 0);
            Assert.AreEqual(0, TileA.getId());
        }
        [TestMethod]
        public void GetColorTest()
        {
            /*
            Test de l'accesseur associé à la variable color.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien le color aux
            bonnes valeurs.
            */
            Tile TileA = new Tile(0, 0, 0);
            Assert.AreEqual(0, TileA.getColor());
        }
        [TestMethod]
        public void GetShapeTest()
        {
            /*
            Test de l'accesseur associé à la variable shape.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien le form aux
            bonnes valeurs.
            */
            Tile TileA = new Tile(0, 0, 0);
            Assert.AreEqual(0, TileA.getShape());
        }
        [TestMethod]
        public void Is_similarTest()
        {
            /*
            Test de la méthode Is_Similar().
            Le but est de vérifier que la méthode nous indique
            bien si la tuile possède un seul attribut en commun
            avec la tuile passé en argument.
            */
            Tile TileA = new Tile(0, 0, 0);
            Tile TileB = new Tile(0, 0, 0);
            Tile TileC = new Tile(0, 1, 0);
            Tile TileD = new Tile(0, 2, 3);
            Assert.AreEqual(true, TileA.Is_similar(TileC));
            Assert.AreEqual(false, TileA.Is_similar(TileB));
            Assert.AreEqual(false, TileA.Is_similar(TileD));
        }
        [TestMethod]
        public void GetHashCodeTest()
        {
            /*
            Test de surcharge de méthode GetHashCode().
            Le but est de vérifier la bonne génération du Hash.
            */
            Tile TileA = new Tile(0, 0, 0);
            Tile TileB = new Tile(0, 2, 5);
            Tile TileC = new Tile(2, 0, 2);
            Tile TileNull = new Tile();
            Assert.AreEqual(222, TileA.GetHashCode());
            Assert.AreEqual(274, TileB.GetHashCode());
            Assert.AreEqual(442, TileC.GetHashCode());
            Assert.AreEqual(111, TileNull.GetHashCode());
        }
        [TestMethod]
        public void OperatorEqualTest()
        {
            /*
            Test de la surcharge de l'opérateur ==
            Le but est de vérifier qu'on peut correctement comparer des
            objets de type Tuiles à l'aide de l'opérateur ==.
            */
            Tile TileA = new Tile(0, 0, 0);
            Tile TileB = new Tile(0, 2, 5);
            Tile TileC = new Tile(0, 0, 0);
            Assert.AreEqual(true, TileA == TileC);
            Assert.AreEqual(false, TileA == TileB);
        }
        [TestMethod]
        public void OperatorUnEqualTest()
        {
            /*
            Test de la surcharge de l'opérateur !=
            Le but est de vérifier qu'on peut correctement comparer des
            objets de type Tuiles à l'aide de l'opérateur !=.
            */
            Tile TileA = new Tile(0, 0, 0);
            Tile TileB = new Tile(0, 2, 5);
            Tile TileC = new Tile(0, 0, 0);
            Assert.AreEqual(false, TileA != TileC);
            Assert.AreEqual(true, TileA != TileB);
        }
        [TestMethod]
        public void EqualTest()
        {
            /*
            Test de la surcharge de la méthode Equals()
            Le but est de vérifier qu'on peut correctement comparer des
            objets de type Tuiles à l'aide de la méthode Equals.
            */
            Tile TileA = new Tile(0, 0, 0);
            Tile TileB = new Tile(0, 2, 5);
            Tile TileC = new Tile(0, 0, 0);
            Assert.AreEqual(true, TileA.Equals(TileC));
            Assert.AreEqual(false, TileA.Equals(TileB));
        }
    }
}
