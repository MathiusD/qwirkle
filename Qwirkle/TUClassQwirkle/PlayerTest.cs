﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassQwirkle;
using Qwirkle.Exceptions;

namespace TUClassQwirkle
{
    [TestClass]
    public class PlayerTest
    {
        /*
        Tests Associés à la Classe Player.
        */
        [TestMethod]
        public void ConstructorTest()
        {
            /*
            Test du Constructeur de Player.
            Le but de ce Test est de savoir si une exception est relevée
            lors de la création de celui-ci.
            */
            Player J1 = new Player("white", "Eudes", 1);
        }
        [TestMethod]
        public void GetColorTest()
        {
            /*
            Test de l'accesseur associé à la variable color.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien le color aux
            bonnes valeurs.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Assert.AreEqual("white", J1.getColor());
        }
        [TestMethod]
        public void GetNameTest()
        {
            /*
            Test de l'accesseur associé à la variable name.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien le name aux
            bonnes valeurs.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Assert.AreEqual("Eudes", J1.getName());
        }
        [TestMethod]
        public void GetScoreTest()
        {
            /*
            Test de l'accesseur associé à la variable score.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien le score aux
            bonnes valeurs.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Assert.AreEqual(0, J1.getScore());
        }
        [TestMethod]
        public void GetAgeTest()
        {
            /*
            Test de l'accesseur associé à la variable age.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien l'age aux
            bonnes valeurs.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Assert.AreEqual(1, J1.getAge());
        }
        [TestMethod]
        public void GetRackTest()
        {
            /*
            Test de l'accesseur associé à la variable rack.
            Le but est de vérifier que ce que renvoie l'accesseur est du
            bon type et que le constructeur initie bien le rack aux
            bonnes valeurs.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Assert.AreEqual(0, J1.getRack().Count);
        }
        [TestMethod]
        public void AddRackTest()
        {
            /*
            Test de la méthode AddRack().
            Le but de ce Test est de savoir si la tuile passée en argument
            est bien ajoutée au rack du joueur.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Tile TileA = new Tile(0, 1, 2);
            J1.AddRack(TileA);
            Assert.AreEqual(1, J1.getRack().Count);
            Assert.AreEqual(TileA, J1.getRack()[0]);
        }
        [TestMethod]
        public void Increase_scoreTest()
        {
            /*
            Test de la méthode Increase_score().
            Le but est de vérifier que la méthode incrémente bien la
            valeur score avec la valeur passée en paramètre.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Assert.AreEqual(0, J1.getScore());
            J1.Increase_score(9);
            Assert.AreEqual(9, J1.getScore());
        }
        [TestMethod]
        public void Missing_TilesTest()
        {
            /*
            Test de la méthode Missing_Tiles().
            Le but est de vérifier que la méthode renvoie bien le
            nombre de tuile manquant dans le rack du joueur
            qui appelle la méthode.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Assert.AreEqual(6, J1.Missing_Tiles());
        }
        [TestMethod]
        public void DrawTest()
        {
            /*
            Test de la méthode Draw().
            Le but est de vérifier que le joueur pioche bien des tuiles
            aléatoire du Board passé en paramètre et que celles-ci sont
            bien débitées du Board en question.
            On vérifie également que la Méthode nous lance bien une 
            Exception du type PlayerDoes'ntExistException() dans le cas
            où on tente de faire piocher un joueur dans un board
            dont il ne fait pas partie.
            On vérifie également que la méthode nous lance également
            une Exception de type StackIsEmptyException() dans le cas où
            on tente de piocher dans le Board alors que son stack est
            vide.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Player J2 = new Player("white", "Eude", 1);
            Player J3 = new Player("black", "Eudes", 1);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J3);
            Board B1 = new Board(LJ1);
            J1.Draw(B1);
            Assert.AreEqual(6, J1.getRack().Count);
            bool test = false;
            try
            {
                J2.Draw(B1);
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new PlayerDoes_ntExistException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void Trade_TilesTest()
        {
            /*
            Test de la méthode Trade_Tiles().
            Le but est de vérifier que le joueur échange bien des tuiles
            aléatoire du Board passé en paramètre et que celles-ci sont
            bien débitées du Board en question et que finalement les
            tuiles échangées sont bien remises dans le stack.
            On vérifie également que la Méthode nous lance bien une 
            Exception du type PlayerDoes'ntExistException() dans le cas
            où on tente de faire piocher un joueur dans un board
            dont il ne fait pas partie.
            On vérifie également que la Méthode nous lance aussi une 
            Exception du type RemoveWrongTilesException() dans le cas
            où on tente d'échanger des tuiles qui n'apartiennent pas
            au rack du joueur qui tente de les échanger.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Player J2 = new Player("black", "Eudes", 0);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            J1.Draw(B1);
            List<Tile> R1 = new List<Tile>();
            for (short i = 0; i < J1.getRack().Count; i++)
            {
                R1.Add(J1.getRack()[i]);
            }
            J1.Trade_Tiles(B1, R1);
            for (short l = 0; l < J1.getRack().Count; l++)
            {
                for (short k = 0; k < R1.Count; k++)
                {
                    Assert.AreEqual(true , R1[k] != J1.getRack()[l]);
                }
            }
        }

        [TestMethod]
        public void Place_TileTest()
        {
            /*
            Test de la méthode Place_Tile().
            Le but est de vérifier que le joueur peut bien placer
            une tuile sur le plateau à l'aide de la méthode
            Place_Tile().
            On vérifie également que la méthode nous renvoie bien
            une Exception de type WrongPositionForTileException dans
            le cas où l'on souhaite placer une tuile sur une case
            non valide.
            On vérifie également que la méthode nous renvoie bien
            une Exception de type PlayerDoes'ntExistException dans
            le cas où l'on souhaite placer une tuile sur board
            auquel nous ne sommes pas renseigné en tant que Joueur.
            On vérifie également que la méthode nous renvoie bien
            une Exception de Type TileOutOfLimitsException dans le
            cas où la tile essaye d'être placé hors du board.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Player J2 = new Player("black", "Eudes", 0);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            J1.Draw(B1);
            bool test = false;
            try
            {
                J1.Place_Tiles(B1, J1.getRack()[0], -1, 56);
            } catch (Exception E)
            {
                if (E.GetType().IsInstanceOfType(new TileOutOfLimitsException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
            List<List<Tile>> LT1 = new List<List<Tile>>(B1.getNbLine()+2);
            for (short i = 0; i < B1.getNbLine()+2; i++)
            {
                List<Tile> Line = new List<Tile>(B1.getNbColumn()+2);
                LT1.Add(Line);
                for (short f = 0; f < B1.getNbColumn()+2; f++)
                {
                    Tile tilenull = new Tile();
                    LT1[i].Add(tilenull);
                }
            }
            LT1[1][1] = J1.getRack()[0];
            List<Tile> R1 = new List<Tile>(J1.getRack().Count-1);
            for (short i = 1; i < J1.getRack().Count; i++)
            {
                R1.Add(J1.getRack()[i]);
            }
            J1.Place_Tiles(B1, J1.getRack()[0], 0, 0);
            Assert.AreEqual(J1.getRack().Count, R1.Count);
            for (short l = 0; l < J1.getRack().Count; l++)
            {
                short m = 0;
                while ((m < R1.Count - 1) && (!(R1[m].Equals(J1.getRack()[l]))))
                {
                    m = (short)(m + 1);
                }
                Assert.AreEqual(true, R1[m].Equals(J1.getRack()[l]));
            }
            Assert.AreEqual(LT1.Count, B1.getBoard().Count);
            for (short i = 0; LT1.Count > i; i++)
            {
                Assert.AreEqual(LT1[i].Count, B1.getBoard()[i].Count);
                if (LT1[i].Count > 0)
                {
                    for (short j = 0; LT1[i].Count > j; j++)
                    {
                        Assert.AreEqual(LT1[i][j], B1.getBoard()[i][j]);
                    }
                }
            }
            test = false;
            try
            {
                J1.Place_Tiles(B1, J1.getRack()[1], 1, 1);
            } catch (Exception E)
            {
                if (E.GetType().IsInstanceOfType(new WrongPositionForTileException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void GetHashCodeTest()
        {
            /*
            Test de la surcharge de la méthode GetHashCode().
            Le but est de vérifier la bonne génération du Hash.
            */
            Player J1 = new Player("white", "Eudes", 1);
            Player J2 = new Player("black", "Blip", 2);
            Player J3 = new Player("green", "Bloup", 42);
            int hash_code = 13;
            hash_code ^= J1.getName().GetHashCode();
            hash_code ^= J1.getColor().GetHashCode();
            hash_code ^= J1.getAge().GetHashCode();
            hash_code ^= J1.getScore().GetHashCode();
            for (short i = 0; i < J1.getRack().Count; i++)
            {
                hash_code ^= J1.getRack()[i].GetHashCode();
            }
            Assert.AreEqual(hash_code, J1.GetHashCode());
            hash_code = 13;
            hash_code ^= J2.getName().GetHashCode();
            hash_code ^= J2.getColor().GetHashCode();
            hash_code ^= J2.getAge().GetHashCode();
            hash_code ^= J2.getScore().GetHashCode();
            for (short i = 0; i < J2.getRack().Count; i++)
            {
                hash_code ^= J2.getRack()[i].GetHashCode();
            }
            Assert.AreEqual(hash_code, J2.GetHashCode());
            hash_code = 13;
            hash_code ^= J3.getName().GetHashCode();
            hash_code ^= J3.getColor().GetHashCode();
            hash_code ^= J3.getAge().GetHashCode();
            hash_code ^= J3.getScore().GetHashCode();
            for (short i = 0; i < J3.getRack().Count; i++)
            {
                hash_code ^= J3.getRack()[i].GetHashCode();
            }
            Assert.AreEqual(hash_code, J3.GetHashCode());
        }
        [TestMethod]
        public void OperatorEqualTest()
        {
            /*
            Test de la surcharge de l'opérateur ==
            Le but est de vérifier qu'on peut correctement comparer des
            objets de type Joueur à l'aide de l'opérateur ==.
            */
            Player J1 = new Player("white", "Eudes", 0);
            Player J2 = new Player("black", "Eudes", 1);
            Player J3 = new Player("white", "Eudes", 0);
            Assert.AreEqual(true, J1 == J3);
            Assert.AreEqual(false, J1 == J2);
        }
        [TestMethod]
        public void OperatorUnEqualTest()
        {
            /*
            Test de la surcharge de l'opérateur !=
            Le but est de vérifier qu'on peut correctement comparer des
            objets de type Joueur à l'aide de l'opérateur !=.
            */
            Player J1 = new Player("white", "Eudes", 0);
            Player J2 = new Player("black", "Eudes", 1);
            Player J3 = new Player("white", "Eudes", 0);
            Assert.AreEqual(false, J1 != J3);
            Assert.AreEqual(true, J1 != J2);
        }
        [TestMethod]
        public void EqualTest()
        {
            /*
            Test de la surcharge de la méthode Equals()
            Le but est de vérifier qu'on peut correctement comparer des
            objets de type Joueur à l'aide de la méthode Equals.
            */
            Player J1 = new Player("white", "Eudes", 0);
            Player J2 = new Player("black", "Eudes", 1);
            Player J3 = new Player("white", "Eudes", 0);
            Assert.AreEqual(true, J1.Equals(J3));
            Assert.AreEqual(false, J1.Equals(J2));
        }
    }
}
