﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qwirkle.Exceptions;
using ClassQwirkle;

namespace TUClassQwirkle
{
    [TestClass]
    public class QwirkleExceptionsTest
    {
        [TestMethod]
        public void BadParametersForTilesConstructorExceptionTest()
        {
            bool test = false;
            try
            {
                throw new BadParametersForTileConstructorException();
            } catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new BadParametersForTileConstructorException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void ExchangeWithWrongTilesExceptionTest()
        {
            bool test = false;
            try
            {
                throw new ExchangeWithWrongTilesException();
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new ExchangeWithWrongTilesException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void PlayerDoes_ntExceptionTest()
        {
            bool test = false;
            try
            {
                throw new PlayerDoes_ntExistException();
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new PlayerDoes_ntExistException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void StackIsEmptyExceptionTest()
        {
            bool test = false;
            try
            {
                throw new StackIsEmptyException();
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new StackIsEmptyException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void RemoveWrongTilesExceptionTest()
        {
            bool test = false;
            try
            {
                throw new RemoveWrongTilesException();
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new RemoveWrongTilesException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void WrongPositionForTileExceptionTest()
        {
            bool test = false;
            Player J1 = new Player("White", "1", 0);
            Player J2 = new Player("White", "1", 0);
            List<Player> LJ1 = new List<Player>();
            LJ1.Add(J1);
            LJ1.Add(J2);
            Board B1 = new Board(LJ1);
            Tile TileA = new Tile();
            short x = 0;
            short y = 0;
            try
            {
                throw new WrongPositionForTileException();
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new WrongPositionForTileException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
            test = false;
            try
            {
                throw new WrongPositionForTileException("Example");
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new WrongPositionForTileException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
            test = false;
            try
            {
                throw new WrongPositionForTileException(B1, TileA, x, y);
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new WrongPositionForTileException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
            test = false;
            try
            {
                throw new WrongPositionForTileException("Example", B1, TileA, x, y);
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new WrongPositionForTileException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void TileOutOfLimitsExceptionTest()
        {
            bool test = false;
            try
            {
                throw new TileOutOfLimitsException();
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new TileOutOfLimitsException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void WrongNumberOfPlayerExceptionTest()
        {
            bool test = false;
            try
            {
                throw new WrongNumberOfPlayerException();
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new WrongNumberOfPlayerException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
            test = false;
            try
            {
                throw new WrongNumberOfPlayerException(1);
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new WrongNumberOfPlayerException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void TileDoes_ntExistExceptionTest()
        {
            bool test = false;
            try
            {
                throw new TileDoes_ntExistException();
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new TileDoes_ntExistException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void BoardIsNotEmptyExceptionTest()
        {
            bool test = false;
            try
            {
                throw new BoardIsNotEmptyException();
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new BoardIsNotEmptyException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
        [TestMethod]
        public void NoTilePlacedInThisTurnExceptionTest()
        {
            bool test = false;
            try
            {
                throw new NoTilePlacedInThisTurnException();
            }
            catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new NoTilePlacedInThisTurnException()))
                {
                    test = true;
                }
            }
            finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }
        }
    }
}
